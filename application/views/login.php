<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistem Penelusuran Bakat Minat - Unimus</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/font.css">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo" style="margin-left: -125px; margin-top: -60px">
    <img src="<?php echo base_url();?>assets/image/logo_sop baru.png" alt="Logo SISOP Online"><br>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg"><span style="color: #777;font-size: 16px;"> Silahkan masuk menggunakan <br>akun Admin anda.</span></p>
    <form action="<?php echo site_url('Login_Controller/aksi_login');?>" method="POST">
      <div class="form-group has-feedback">
        <input type="nip" name="nip" class="form-control" placeholder="Pengguna">
        <span class="fa fa-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" placeholder="Sandi">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
          <?php
          $salah = '';
          if($this->session->flashdata('salah') !== null)
          {
            $slh = $this->session->flashdata('salah');
          }else{
            $slh = ($salah != '') ? $salah : '';   
          }
          ?>
      <div align="center" style="margin-bottom: 5px"><font style="color:red"><?= $slh ?></font></div>
      <div class="form-group has-feedback">
        <input type="submit" class="btn btn-primary" value="Masuk" style="width: 100%;">
      </div>
      <div class="form-group has-feedback">
        <a href="" type="button" class="btn btn-primary" style="width: 100%;">Pelaporan Studi Lanjut</a>
      </a>
    </div>
    </form>
<center><span style="font-size: 10px;color: #777;">&copy 2019 Kabupaten Kendal</span></center>
    <!-- /.social-auth-links -->
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/plugins/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
</body>
</html>
