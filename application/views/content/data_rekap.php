<?php
  $blok=$this->uri->segment(5);
  $id_jenis=$this->uri->segment(4);
  $val=$this->Sop_Model->qw("*","data_pernyataan","WHERE id_jenis='$id_jenis'")->result();
  $jenis_kues=$this->Sop_Model->qw("*","jenis_kuesioner","WHERE id_jenis='$id_jenis'")->row_array();
  $nama_blok=$this->Sop_Model->qw("*","mk","WHERE kdmk='$blok'")->row_array();
  $nim = $this->session->userdata('nama');
  $open='Sop_Controller/simpan_pernyataan';
  $ta_berjalan = '2021/2022';
  $gs_berjalan = '2';

  $tmp_pt=$this->Sop_Model->qw("komentar","data_jawaban","WHERE jenis_kuesioner='$id_jenis' AND komentar!=''")->result();
?>
<section class="content-header">
      <h1>
        Pengisian Kuesioner (<?php echo $jenis_kues['jenis_kuesioner']?>)
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Form</a></li>
        <li class="active">Pengisian Kuesioner</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">

        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <form role="form" class="form-horizontal" action="<?php echo site_url($open);?>" method="POST" enctype="multipart/form-data">
              <input name="jenis_kuesioner" value="<?php echo $id_jenis?>" type="hidden">
              <input name="nim" value="<?php echo $nim?>" type="hidden">
              <input name="blok" value="<?php echo $blok?>" type="hidden">
              <input name="ta" value="<?php echo $ta_berjalan?>" type="hidden">
              <input name="gs" value="<?php echo $gs_berjalan?>" type="hidden">
              <div class="box-body">
                <div class="row">
                <div class="col-md-12">
                <?php

                  $no=0;

                  foreach($val as $tampil){

                  $no++;

                ?>
                <input name="no_pernyataan[]" value="<?php echo $tampil->nomor?>" type="hidden">
                <p><?php echo $tampil->nomor?>. <?php echo $tampil->pernyataan?></p>
                <div style="margin-left: 15px">
                  <?php
                  $jwb=$this->Sop_Model->qw("*","jawaban_kuesioner","WHERE id_jenis='$id_jenis' ORDER BY nilai_jawaban ASC")->result();
                  ?>
                  <table class="table table-bordered table-striped display">
                  <?php
                  foreach($jwb as $jw){
                    ?>
                    <tr>
                      <td width="20%"><?php echo $jw->jawaban?></td>
                      <td width="10%">
                        <?php
                        $ambil_jawab=$this->Sop_Model->qw("*","data_jawaban","WHERE jenis_kuesioner='$id_jenis' AND id_pernyataan='$tampil->nomor' AND jawaban='$jw->nilai_jawaban'")->num_rows();
                        echo $ambil_jawab;
                        $ambil_semua_jawab = $this->Sop_Model->qw("*","data_jawaban","WHERE jenis_kuesioner='$id_jenis' AND id_pernyataan='$tampil->nomor'")->num_rows();
                        echo "/";
                        echo $ambil_semua_jawab;
                        $presentase = ($ambil_jawab/$ambil_semua_jawab)*100;
                        ?>
                      </td>
                      <td>
                        <div class="progress">
                          <div class="progress-bar" role="progressbar" style="width: <?php echo number_format($presentase,2)?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><div align="left" style="margin-left: 10px"><?php echo number_format($presentase,2)?> %</div></div>
                        </div>
                      </td>
                    </tr>
                    <?php
                  }
                  ?>
                  </table>
                  <!--
                  <input type="radio" id="male" name="pernyataan[<?php echo $tampil->nomor?>]" value="1" required>
                  <label for="male">Ya</label><br>
                  <input type="radio" id="female" name="pernyataan[<?php echo $tampil->nomor?>]" value="0">
                  <label for="female">Tidak</label>-->
                </div>
                <br>
                <?php
                }
                ?>

              </div>

              <div class="col-md-12">
                <h3>Komentar</h3><br>
                <table id="example2" class="table table-bordered table-striped display">

                  <thead>

                  <tr>

                    <th width="10%">No</th>

                    <th>Komentar</th>

                  </thead>

                  <tbody>

                  <?php

                    $no=0;

                    foreach($tmp_pt as $tampil){

                    $no++;

                  ?>

                  <tr>

                    <td><?php echo $no;?></td>
                    <td><?php echo $tampil->komentar;?></td>
                  </tr>

                  <?php } ?>

                  </tbody>

                </table>
              </div>

              </div>
              </div>
              </div>
              <div class="box-footer">
                  <a href="<?php echo site_url('Sop_Controller/page/data_kuisioner/');?>" class="btn btn-md btn-danger"> Kembali</a>
                  <?php
                  if($id_jenis>1)
                  {
                    $data_before = $id_jenis-1;
                    ?>
                    <a href="<?php echo site_url('Sop_Controller/page/data_rekap/'.$data_before);?>" class="btn btn-md btn-warning">&#10094; Sebelumnya</a>
                    <?php
                  }
                  ?>
                  <?php
                  if($id_jenis<5)
                  {
                    $data_next = $id_jenis+1;
                    ?>
                    <a href="<?php echo site_url('Sop_Controller/page/data_rekap/'.$data_next);?>" class="btn btn-md btn-info">Berikutnya &#10095;</a>
                    <?php
                  }
                  ?>
                  
              </div>
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
         
          <!-- /.box -->

          
          <!-- /.box -->

          <!-- Input addon -->
          
          <!-- /.box -->
        </div>
      </div>
    </section>