<section class="content-header">

      <h1>
        Data Peserta Survey
      </h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li><a href="#">Data</a></li>

        <li class="active">Data Peserta Survey</li>

      </ol>

</section>



   <section class="content">

      <div class="row">

        <div class="col-xs-12">

          <div class="box box-primary">

            <!-- /.box-header -->

            <div class="box-header" style="margin-top: 20px;">
              <div align="right" style="margin-right: 20px">
                <a href="<?php echo site_url("Sop_Controller/cetak_laporan"); ?>" class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-file-excel-o"></i> Export</a>
              </div>
            </div>

            <div class="box-body">

              <table id="example2" class="table table-bordered table-striped display">

                <thead>

                <tr>

                  <th>No</th>

                  <th>Nama</th>

                  <th>Sekolah</th>
                  <th>Kelas</th>
                  <th>Handphone</th>
                  <th>Email</th>
                  <th>Waktu Input</th>
                  <th>Hasil</th>
                </tr>

                </thead>

                <tbody>

                <?php

                  $no=0;

                  foreach($tmp_pt as $tampil){

                  $no++;

                ?>

                <tr>

                  <td><?php echo $no;?></td>
                  <td><?php echo $tampil->nama;?></td>
                  <td><?php echo $tampil->sekolah;?></td>
                  <td><?php echo $tampil->kelas;?></td>
                  <td><?php echo $tampil->hp;?></td>
                  <td><?php echo $tampil->email;?></td>
                  <td><?php echo date("d-m-Y", strtotime($tampil->waktu_input));?>
                  </td>

                  <td>
                    <?php
                    $id_peserta = $tampil->id;
                    $hitung =$this->Sop_Model->qw("*","data_jawaban","WHERE id_siswa='$id_peserta'")->num_rows();
                    if($hitung=='80')
                    {
                      ?>
                      <a href="<?php echo site_url('Sop_Controller/page/hasil_survey/'.$tampil->id.'/1');?>" class="btn btn-sm btn-success"><i class="fa fa-pencil-square-o"></i> Lihat</a>
                      <a href="<?php echo site_url('Pendaftaran_Controller/cetak_bukti/'.$tampil->id);?>" class="btn btn-sm btn-warning" target="_blank"><i class="fa fa-file-pdf-o"></i> Cetak</a>
                      <?php
                    }else{
                      echo "<font size=2 color=red>Jawaban Tidak Lengkap</font>";
                    }
                    ?>
                    
                  </td>

                </tr>

                <?php } ?>

                </tbody>

              </table>

            </div>

            <!-- /.box-body -->

          </div>

          <!-- /.box -->

        </div>

        <!-- /.col -->

      </div>

      <!-- /.row -->

    </section>