<?php
          $id_siswa=$this->uri->segment(4);
          $admin=$this->uri->segment(5);
          $val=$this->Sop_Model->qw("*","data_jawaban","WHERE id_siswa='$id_siswa'")->result();
          $data_peserta=$this->Sop_Model->qw("*","data_peserta","WHERE id='$id_siswa'")->row_array();
          $lin=0;
          $mu=0;
          $lm=0;
          $nter=0;
          $sp=0;
          $ntra=0;
          $bk=0;
          $nat=0;
          foreach($val as $tampil){
            $pernyataan=$this->Sop_Model->qw("*","data_pernyataan","WHERE nomor='$tampil->id_pernyataan'"
        )->row_array();
            if($tampil->jawaban=='1' AND $pernyataan['kecerdasan']=='LIN')
              $lin++;
            if($tampil->jawaban=='1' AND $pernyataan['kecerdasan']=='MU')
              $mu++;
            if($tampil->jawaban=='1' AND $pernyataan['kecerdasan']=='LM')
              $lm++;
            if($tampil->jawaban=='1' AND $pernyataan['kecerdasan']=='NTER')
              $nter++;
            if($tampil->jawaban=='1' AND $pernyataan['kecerdasan']=='SP')
              $sp++;
            if($tampil->jawaban=='1' AND $pernyataan['kecerdasan']=='NTRA')
              $ntra++;
            if($tampil->jawaban=='1' AND $pernyataan['kecerdasan']=='BK')
              $bk++;
            if($tampil->jawaban=='1' AND $pernyataan['kecerdasan']=='NAT')
              $nat++;
          }

          $p_lin = $lin/10*100;
          $p_mu = $mu/10*100;
          $p_lm = $lm/10*100;
          $p_nter = $nter/10*100;
          $p_sp = $sp/10*100;
          $p_ntra = $ntra/10*100;
          $p_bk = $bk/10*100;
          $p_nat = $nat/10*100;

          $data_prodi=array();
          //masukkan kondisi prodi
          //FK dihilangkan
          /*
          if($p_lm>80 && $p_bk>30 && $p_nter>50 && $p_nat>50)
            $data_prodi[1] = 1;
          if($p_lm>70 && $p_bk>30 && $p_nter>50 && $p_nat>50)
            $data_prodi[2] = 2;*/
          if($p_lm>60 && $p_nter>50 && $p_nat>50)
            $data_prodi[3] = 3;
          if($p_lm>70 && $p_nter>70 && $p_nat>60)
            $data_prodi[4] = 4;
          if($p_lm>60 && $p_nter>50 && $p_nat>60)
            $data_prodi[5] = 5;
          if($p_lm>60 && $p_nter>50 && $p_nat>60)
            $data_prodi[6] = 6;
          if($p_lm>60 && $p_sp>40 && $p_nter>40)
            $data_prodi[7] = 7;
          if($p_lm>50 && $p_sp>50 && $p_nter>50 && $p_ntra>30 && $p_nat>30)
            $data_prodi[8] = 8;
          if($p_lm>60 && $p_nter>50 && $p_ntra>30)
            $data_prodi[9] = 9;
          if($p_lm>65)
            $data_prodi[10] = 10;
          if($p_lm>65)
            $data_prodi[11] = 11;
          if($p_lm>40 && $p_sp>30 && $p_nter>30)
            $data_prodi[12] = 12;
          if($p_lm>40 && $p_sp>30 && $p_nter>30)
            $data_prodi[13] = 13;
          if($p_lin>60 && $p_nter>50)
            $data_prodi[14] = 14;
          if($p_lin>60 && $p_nter>60)
            $data_prodi[15] = 15;
          if($p_lm>50 && $p_nter>50 && $p_nat>60)
            $data_prodi[16] = 16;
          if($p_lm>60 && $p_nter>50)
            $data_prodi[17] = 17;
          if($p_lm>60 && $p_nter>70 && $p_nat>60)
            $data_prodi[18] = 18;
          if($p_lm>50 && $p_nter>60 && $p_nat>60)
            $data_prodi[19] = 19;
          if($p_lm>60 && $p_nter>70 && $p_nat>60)
            $data_prodi[20] = 20;
          if($p_lm>60 && $p_nter>70 && $p_nat>60)
            $data_prodi[21] = 21;
          if($p_lm>60 && $p_nter>70 && $p_nat>60)
            $data_prodi[22] = 22;

          if(empty($data_prodi))
          {
            $data_prodi[3] = 3;
            $data_prodi[5] = 5;
            $data_prodi[6] = 6;
            $data_prodi[12] = 12;
            $data_prodi[13] = 13;
            $data_prodi[14] = 14;
            $data_prodi[15] = 15;
            $data_prodi[16] = 16;
            $data_prodi[19] = 19;
            $data_prodi[20] = 20;
          }

          $data[] = array('nilai' => $p_lin, 'tipe' => 'LIN');
          $data[] = array('nilai' => $p_mu, 'tipe' => 'MU');
          $data[] = array('nilai' => $p_lm, 'tipe' => 'LM');
          $data[] = array('nilai' => $p_nter, 'tipe' => 'NTER');
          $data[] = array('nilai' => $p_sp, 'tipe' => 'SP');
          $data[] = array('nilai' => $p_ntra, 'tipe' => 'NTRA');
          $data[] = array('nilai' => $p_bk, 'tipe' => 'BK');
          $data[] = array('nilai' => $p_nat, 'tipe' => 'NAT');

          foreach ($data as $key => $row) {
              $nilai[$key]  = $row['nilai'];
              $tipe[$key] = $row['tipe'];
          }

          $nilai  = array_column($data, 'nilai');
          array_multisort($nilai, SORT_DESC, $data);

          foreach ($data[0] as $key => $value) {
            if ($key == "nilai")
              $nilai1 = $value;
            if ($key == "tipe")
              $tipe1 = $value;
          }

          foreach ($data[1] as $key => $value) {
            if ($key == "nilai")
              $nilai2 = $value;
            if ($key == "tipe")
              $tipe2 = $value;
          }

          foreach ($data[2] as $key => $value) {
            if ($key == "nilai")
              $nilai3 = $value;
            if ($key == "tipe")
              $tipe3 = $value;
          }

?>
<section class="content-header">
      <h1>
        <?php
        if($admin=='1')
          echo "Hasil Survey";
        else
          echo "Langkah 3 : Hasil Survey"
        ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Form</a></li>
        <li class="active">Hasil Survey</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <form role="form" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
              <input name="status" value="" type="hidden">
              <div class="box-body">
                <div class="row">
                <div class="col-md-11">
                <div class="form-group">
                  <label class="col-sm-2">Nama</label>
                  <div class="col-sm-9">
                    <?php echo $data_peserta['nama']?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2">Asal Sekolah</label>
                  <div class="col-sm-9">
                    <?php echo $data_peserta['sekolah']?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2">No.HP / Email</label>
                  <div class="col-sm-9">
                    <?php echo $data_peserta['hp']?> / <?php echo $data_peserta['email']?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2">Nilai Komponen</label>
                  <div class="col-sm-3">
                    <?php
                    echo "Linguistic = ".$p_lin."<br>";
                    echo "Musical = ".$p_mu."<br>";
                    echo "Logical-Mathematical = ".$p_lm."<br>";
                    echo "Interpersonal = ".$p_nter."<br>";
                    ?>
                  </div>
                  <div class="col-sm-4">
                    <?php
                    echo "Spatial = ".$p_sp."<br>";
                    echo "Intrapersonal = ".$p_ntra."<br>";
                    echo "Bodily-Kinesthetic = ".$p_bk."<br>";
                    echo "Naturalist = ".$p_nat."<br>";
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2">Tiga Kecerdasan Menonjol</label>
                  <div class="col-sm-10">
                    <?php
                    $ambil_cerdas=$this->Sop_Model->qw("*","kecerdasan","WHERE istilah='$tipe1'")->row_array();
                    echo "<b>1. ";
                    echo $ambil_cerdas['nama_kecerdasan'];
                    echo " (".$nilai1.")</b>";
                    echo "<br>";
                    echo $ambil_cerdas['penjelasan'];
                    echo "<br>";
                    echo "<b>Kemungkinan Karier : </b>";
                    echo $ambil_cerdas['karir'];
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2"></label>
                  <div class="col-sm-10">
                    <?php
                    $ambil_cerdas2=$this->Sop_Model->qw("*","kecerdasan","WHERE istilah='$tipe2'")->row_array();
                    echo "<b>2. ";
                    echo $ambil_cerdas2['nama_kecerdasan'];
                    echo " (".$nilai2.")</b>";
                    echo "<br>";
                    echo $ambil_cerdas2['penjelasan'];
                    echo "<br>";
                    echo "<b>Kemungkinan Karier : </b>";
                    echo $ambil_cerdas2['karir'];
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2"></label>
                  <div class="col-sm-10">
                    <?php
                    $ambil_cerdas3=$this->Sop_Model->qw("*","kecerdasan","WHERE istilah='$tipe3'")->row_array();
                    echo "<b>3. ";
                    echo $ambil_cerdas3['nama_kecerdasan'];
                    echo " (".$nilai3.")</b>";
                    echo "<br>";
                    echo $ambil_cerdas3['penjelasan'];
                    echo "<br>";
                    echo "<b>Kemungkinan Karier : </b>";
                    echo $ambil_cerdas3['karir'];
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2">Program Studi Pilihan di Unimus Yang Sesuai</label>
                  <div class="col-sm-9">
                    <?php
                    //AMBIL NAMA PRODI
                    foreach ($data_prodi as $key => $value) {
                      $ambil_nama=$this->Sop_Model->qw("prodi","prodi","WHERE id='$value'")->row_array();
                      echo $ambil_nama['prodi'];
                      echo "<br>";
                    }
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-0"></label>
                  <div class="col-sm-12">
                    <b><font size="4" color="#1b7abd"> Hasil tes dapat dipakai dasar Anda diterima sebagai mahasiswa baru melalui jalur bebas tes tulis (JBTT)</font></b>&nbsp;&nbsp;<a href="https://reg.pmb.unimus.ac.id/index.php?hal=em5M1Y8nHG8NIkPTW5o0WuwJrpU8R7Yyl@MDWOY2vE5E7FR6tmC9iNnVTLPsXVVd&j=8" class="btn btn-primary" target="_blank" ></i> Daftar Sekarang</a>
                  </div>
                </div>
                </div>
              </div>
              </div>
              </div>

              <!-- /.box-body -->

              <div class="box-footer">
                <?php
                if($admin=='1')
                {
                  ?>
                  <a href="<?php echo site_url('Sop_Controller/page/data_peserta');?>" class="btn btn-danger"><i class="fa fa-close"></i> Kembali</a>
                  <?php
                }else{
                  ?>
                  <a href="<?php echo site_url('Pendaftaran_Controller/cetak_bukti/'.$id_siswa);?>" class="btn btn-primary" target="_blank" ><i class="fa fa-file-pdf-o"></i> Cetak Bukti</a>
                  <?php
                }
                ?>
              </div>
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
         
          <!-- /.box -->

          
          <!-- /.box -->

          <!-- Input addon -->
          
          <!-- /.box -->
        </div>
      </div>
    </section>