<?php
  $blok=$this->uri->segment(5);
  $id_peserta=$this->uri->segment(4);
  $val=$this->Sop_Model->qw("*","data_pernyataan_vmts","ORDER BY id_pernyataan ASC")->result();
  $open='Pendaftaran_Controller/simpan_pernyataan_vmts';
  // $ta_berjalan = '2021/2022';
  // $gs_berjalan = '2';
?>
<section class="content-header">
      <h1>
        <h1>
          Langkah 2 : Pengisian Kuesioner VMTS
        </h1>
      </h1>
      <!-- <?php echo $nama_blok['nama_mk1']?> - <?php echo $ta_berjalan?> (Genap) -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Form</a></li>
        <li class="active">Pengisian Kuesioner</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">

        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <form role="form" class="form-horizontal" action="<?php echo site_url($open);?>" method="POST" enctype="multipart/form-data">
              <input name="id_peserta" value="<?php echo $id_peserta?>" type="hidden">
              <div class="box-body">
                <div class="row">
                <div class="col-md-12">
                <?php

                  $no=0;

                  foreach($val as $tampil){

                  $no++;

                ?>
                <input name="no_pernyataan[]" value="<?php echo $tampil->nomor?>" type="hidden">
                <p><?php echo $tampil->nomor?>. <?php echo $tampil->pernyataan?></p>
                <div style="margin-left: 15px">
                  <table width="100%">
                  <?php
                  $jwb=$this->Sop_Model->qw("*","jawaban_kuesioner_vmts","WHERE id_jenis='$tampil->id_jenis' ORDER BY nilai_jawaban ASC")->result();
                  foreach($jwb as $jw){
                    ?>
                    <!-- <div class="col-md-1"> -->
                      <tr>
                        <td width="1%" valign="top">
                      <input type="radio" id="<?php echo $jw->nilai_jawaban?>" name="pernyataan[<?php echo $tampil->nomor?>]" value="<?php echo $jw->nilai_jawaban?>" required>
                        </td>
                      <td width="2%"></td>
                    <!-- </div> -->
                    <!-- <div class="col-md-11" align="left"> -->
                      <td width="96%" valign="top">
                      <div><label for="male"><?php echo $jw->jawaban?></label></div>
                    </td>
                    <!-- </div> -->
                      </tr>
                    
                    <?php
                  }
                  ?>
                  </table>
                  <!--
                  <input type="radio" id="male" name="pernyataan[<?php echo $tampil->nomor?>]" value="1" required>
                  <label for="male">Ya</label><br>
                  <input type="radio" id="female" name="pernyataan[<?php echo $tampil->nomor?>]" value="0">
                  <label for="female">Tidak</label>-->
                </div>
                <br>
                <?php
                }
                ?>
                <br>
                <div class="col-md-6">
                  15. Bila berkenan, tuliskan saran Anda dalam mewujudkan Visi dan Misi FK UNIMUS
                  <textarea class="form-control" name="komentar"></textarea>
                </div>
              </div>
              </div>
              </div>
              </div>
              <div class="box-footer">
                  <button type="submit" name="" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
         
          <!-- /.box -->

          
          <!-- /.box -->

          <!-- Input addon -->
          
          <!-- /.box -->
        </div>
      </div>
    </section>