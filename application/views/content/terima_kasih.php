<section class="content-header">
      <h1>
        Terima kasih
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Input</a></li>
        <li class="active">Terima Kasih</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <form role="form" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
              <div class="box-body">
                <div class="row">
                <div class="col-md-12">
                <div class="form-group">
                  <div class="col-sm-12">
                    Terima kasih sudah ikut berpartisipasi dalam pengisian Survey Pemahaman Visi Misi Tujuan dan Sasaran FK Unimus&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo site_url('');?>" class="btn btn-sm btn-danger">Kembali</a>
                  </div>
                </div>
                </div>
              </div>
              </div>
              </div>

              <!-- /.box-body -->
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
         
          <!-- /.box -->

          
          <!-- /.box -->

          <!-- Input addon -->
          
          <!-- /.box -->
        </div>
      </div>
    </section>