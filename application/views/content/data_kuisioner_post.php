<?php
$username = $this->session->userdata('nama');
$us=$this->Sop_Model->qw("*","status_peserta","WHERE nim='$username'")->row_array(); 
?>
<section class="content-header">
      <h1>
        Data Post Kuesioner
      </h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li><a href="#">Data</a></li>

        <li class="active">Data Post Kuisioner</li>

      </ol>

</section>



   <section class="content">

      <div class="row">

        <div class="col-xs-8">

          <div class="box box-primary">

            <!-- /.box-header 

            <div class="box-header" style="margin-top: 20px;">
              <div align="right" style="margin-right: 20px">
                <a href="" class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-file-excel-o"></i> Export</a>
              </div>
            </div>-->

            <div class="box-body">

              <?php
              if($us['pre1']=='0' && $us['pre2']=='0' && $us['pre3']=='0'){
                ?>
                <div style="margin-top:10px;margin-botton:20px;">
                  Anda <b>tidak menunjukkan masalah psikososial</b>, Silahkan dapat melihat edukasi kesehatan jiwa secara umum <a href="<?php echo site_url('Sop_Controller/page/data_edukasi/');?>" class="btn btn-sm btn-success"><i class="fa fa-bullhorn"></i> Lihat</a>
                </div>
                <?php
              }else{
                ?>
                <table id="example2" class="table table-bordered table-striped display">

                  <thead>

                  <tr>

                    <!-- <th>No</th> -->
                    <th>Nama Kuisioner</th>
                    <th>Aksi</th>
                  </tr>

                  </thead>

                  <tbody>

                  <?php
                  if($us['pre1']=='1')
                  {
                    ?>
                    <tr>

                      <!-- <td><?php echo $tampil->id_jenis;?></td> -->
                      <td>Kuesioner Depresi</td>
                      </td>
                      <td>
                          <?php
                          $cek1=$this->Sop_Model->qw("*","data_jawaban_post","WHERE nim='$username' AND jenis_kuesioner='1'")->num_rows();
                          if($cek1=='0')
                          {
                            ?>
                            <a href="<?php echo site_url('Sop_Controller/page/data_checklist_post/1/');?>" class="btn btn-sm btn-success"><i class="fa fa-pencil-square-o"></i> Isi Kuesioner</a>
                            <?php
                          }else{
                            ?>
                            <b>Hasil : <?php
                            $sesudah1 = $us['sesudah1'];
                            if($sesudah1>=0 && $sesudah1<=9)
                              echo "Normal";
                            elseif($sesudah1>=10 && $sesudah1<=13)
                              echo "Ringan";
                            elseif($sesudah1>=14 && $sesudah1<=20)
                              echo "Sedang";
                            elseif($sesudah1>=21 && $sesudah1<=27)
                              echo "Berat";
                            elseif($sesudah1>=28)
                              echo "Sangat Berat";
                            echo " (";
                            echo $us['sesudah1'];
                            echo ")";
                            ?></b><br>
                            Rekomendasi : <?php
                            if($sesudah1>=14)
                              echo "Segera konsultasi dengan Guru BK";
                            else
                            {
                              ?>
                              Silahkan dapat melihat edukasi kesehatan jiwa secara umum <a href="<?php echo site_url('Sop_Controller/page/data_edukasi/');?>" class="btn btn-sm btn-success"><i class="fa fa-bullhorn"></i> Lihat</a>
                              <?php
                            }  
                          } 
                          ?>
                          
                        
                      </td>

                    </tr>
                    <?php
                  }
                  ?>

                  <?php
                  if($us['pre2']=='1')
                  {
                    ?>
                    <tr>

                      <!-- <td><?php echo $tampil->id_jenis;?></td> -->
                      <td>Kuesioner Kecemasan</td>
                      </td>
                      <td>
                          <?php
                          $cek2=$this->Sop_Model->qw("*","data_jawaban_post","WHERE nim='$username' AND jenis_kuesioner='2'")->num_rows();
                          if($cek2=='0')
                          {
                            ?>
                            <a href="<?php echo site_url('Sop_Controller/page/data_checklist_post/2/');?>" class="btn btn-sm btn-success"><i class="fa fa-pencil-square-o"></i> Isi Kuesioner</a>
                            <?php
                          }else{
                            ?>
                            <b>Hasil : <?php
                            $sesudah2 = $us['sesudah2'];
                            if($sesudah2>=0 && $sesudah2<=7)
                              echo "Normal";
                            elseif($sesudah2>=8 && $sesudah2<=9)
                              echo "Ringan";
                            elseif($sesudah2>=10 && $sesudah2<=14)
                              echo "Sedang";
                            elseif($sesudah2>=15 && $sesudah2<=19)
                              echo "Berat";
                            elseif($sesudah2>=20)
                              echo "Sangat Berat";
                            echo " (";
                            echo $us['sesudah2'];
                            echo ")";
                            ?></b><br>
                            Rekomendasi : <?php
                            if($sesudah2>=10)
                              echo "Segera konsultasi dengan Guru BK";
                            else
                            {
                              ?>
                              Silahkan dapat melihat edukasi kesehatan jiwa secara umum <a href="<?php echo site_url('Sop_Controller/page/data_edukasi/');?>" class="btn btn-sm btn-success"><i class="fa fa-bullhorn"></i> Lihat</a>
                              <?php
                            }  
                          } 
                          ?>
                        
                      </td>

                    </tr>
                    <?php
                  }
                  ?>

                  <?php
                  if($us['pre3']=='1')
                  {
                    ?>
                    <tr>

                      <!-- <td><?php echo $tampil->id_jenis;?></td> -->
                      <td>Kuesioner Stress</td>
                      </td>
                      <td>
                          <?php
                          $cek3=$this->Sop_Model->qw("*","data_jawaban_post","WHERE nim='$username' AND jenis_kuesioner='3'")->num_rows();
                          if($cek3=='0')
                          {
                            ?>
                            <a href="<?php echo site_url('Sop_Controller/page/data_checklist_post/3/');?>" class="btn btn-sm btn-success"><i class="fa fa-pencil-square-o"></i> Isi Kuesioner</a>
                            <?php
                          }else{
                            ?>
                            <b>Hasil : <?php
                            $sesudah3 = $us['sesudah3'];
                            if($sesudah3>=0 && $sesudah3<=14)
                              echo "Normal";
                            elseif($sesudah3>=15 && $sesudah3<=18)
                              echo "Ringan";
                            elseif($sesudah3>=19 && $sesudah3<=25)
                              echo "Sedang";
                            elseif($sesudah3>=26 && $sesudah3<=33)
                              echo "Berat";
                            elseif($sesudah3>=34)
                              echo "Sangat Berat";
                            echo " (";
                            echo $us['sesudah3'];
                            echo ")";
                            ?></b><br>
                            Rekomendasi : <?php
                            if($sesudah3>=10)
                              echo "Segera konsultasi dengan Guru BK";
                            else
                            {
                              ?>
                              Silahkan dapat melihat edukasi kesehatan jiwa secara umum <a href="<?php echo site_url('Sop_Controller/page/data_edukasi/');?>" class="btn btn-sm btn-success"><i class="fa fa-bullhorn"></i> Lihat</a>
                              <?php
                            }
                          } 
                          ?>
                        
                      </td>

                    </tr>
                    <?php
                  }
                  ?>
                  
                  </tbody>

                </table>

                <?php
              }
              ?>

              
            </div>

            <!-- /.box-body -->

          </div>

          <!-- /.box -->

        </div>

        <!-- /.col -->

      </div>

      <!-- /.row -->

    </section>