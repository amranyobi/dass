<?php
$ta_berjalan = '2021/2022';
$gs_berjalan = '2';
$username = $this->session->userdata('nama');
$us=$this->Sop_Model->qw("nilai.*, mk.nama_mk1","nilai, mk","WHERE mk.kdmk=nilai.kdmk AND nilai.nim = '$username' AND nilai.ta='$ta_berjalan' AND nilai.gs='$gs_berjalan'")->result(); 
?>
<section class="content-header">
      <h1>
        Data Blok TA 2021/2022 (Genap)
      </h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li><a href="#">Data</a></li>

        <li class="active">Data Blok</li>

      </ol>

</section>



   <section class="content">

      <div class="row">

        <div class="col-xs-12">

          <div class="box box-primary">

            <!-- /.box-header 

            <div class="box-header" style="margin-top: 20px;">
              <div align="right" style="margin-right: 20px">
                <a href="" class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-file-excel-o"></i> Export</a>
              </div>
            </div>-->

            <div class="box-body">

              <table id="example2" class="table table-bordered table-striped display">

                <thead>

                <tr>

                  <th>No</th>

                  <th>Kode Blok</th>
                  <th>Nama Blok</th>
                  <th>Kuisioner Terinput</th>
                  <th></th>
                </tr>

                </thead>

                <tbody>

                <?php

                  $no=0;

                  foreach($us as $tampil){

                  $no++;

                ?>

                <tr>

                  <td><?php echo $no;?></td>
                  <td><?php echo $tampil->kdmk;?></td>
                  <td><?php echo $tampil->nama_mk1;?></td>
                  <td>
                    <?php
                    $jenis_kues=$this->Sop_Model->qw("*","data_jawaban","WHERE ta='$ta_berjalan' AND gs='$gs_berjalan' AND nim='$username' AND blok='$tampil->kdmk' GROUP BY jenis_kuesioner")->num_rows();
                    if($jenis_kues=='0')
                      echo "<font color=red>Belum Diinputkan</font>";
                    else
                      echo $jenis_kues;
                    ?>
                  </td>
                  </td>

                  <td>
                      <a href="<?php echo site_url('Sop_Controller/page/data_checklist/1/'.$tampil->kdmk);?>" class="btn btn-sm btn-success"><i class="fa fa-pencil-square-o"></i> Isi Kuesioner</a>
                      <!--<a href="<?php echo site_url('Pendaftaran_Controller/cetak_bukti/'.$tampil->id);?>" class="btn btn-sm btn-warning" target="_blank"><i class="fa fa-file-pdf-o"></i> Cetak</a>-->
                    
                  </td>

                </tr>

                <?php } ?>

                </tbody>

              </table>

            </div>

            <!-- /.box-body -->

          </div>

          <!-- /.box -->

        </div>

        <!-- /.col -->

      </div>

      <!-- /.row -->

    </section>