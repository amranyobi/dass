<?php
  $val=$this->Sop_Model->qw("*","data_pernyataan_vmts","ORDER BY id_pernyataan ASC")->result();

  $tmp_pt=$this->Sop_Model->qw("komentar","data_jawaban","WHERE komentar!='' GROUP BY komentar")->result();
?>
<section class="content-header">
      <h1>
        Pengisian Kuesioner Pemahaman VMTS
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Form</a></li>
        <li class="active">Pengisian Kuesioner</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">

        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <form role="form" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
              <div class="box-body">
                <div class="row">
                <div class="col-md-12">
                <?php

                  $no=0;

                  foreach($val as $tampil){

                  $no++;

                ?>
                <input name="no_pernyataan[]" value="<?php echo $tampil->nomor?>" type="hidden">
                <p><?php echo $tampil->nomor?>. <?php echo $tampil->pernyataan?></p>
                <div style="margin-left: 15px">
                  <?php
                  $jwb=$this->Sop_Model->qw("*","jawaban_kuesioner_vmts","WHERE id_jenis='$tampil->id_jenis' ORDER BY nilai_jawaban ASC")->result();
                  ?>
                  <table class="table table-bordered table-striped display">
                  <?php
                  foreach($jwb as $jw){
                    ?>
                    <tr>
                      <td width="20%"><?php echo $jw->jawaban?></td>
                      <td width="10%">
                        <?php
                        $ambil_jawab=$this->Sop_Model->qw("*","data_jawaban_vmts","WHERE id_pernyataan='$tampil->nomor' AND jawaban='$jw->nilai_jawaban'")->num_rows();
                        echo $ambil_jawab;
                        $ambil_semua_jawab = $this->Sop_Model->qw("*","data_jawaban_vmts","WHERE id_pernyataan='$tampil->nomor'")->num_rows();
                        echo "/";
                        echo $ambil_semua_jawab;
                        if($ambil_jawab=='0' || $ambil_semua_jawab=='0')
                        {
                          $presentase =  0;
                        }else{
                          $presentase = ($ambil_jawab/$ambil_semua_jawab)*100; 
                        }
                        
                        ?>
                      </td>
                      <td>
                        <div class="progress">
                          <div class="progress-bar" role="progressbar" style="width: <?php echo number_format($presentase,2)?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><div align="left" style="margin-left: 10px"><?php echo number_format($presentase,2)?> %</div></div>
                        </div>
                      </td>
                    </tr>
                    <?php
                  }
                  ?>
                  </table>
                  <!--
                  <input type="radio" id="male" name="pernyataan[<?php echo $tampil->nomor?>]" value="1" required>
                  <label for="male">Ya</label><br>
                  <input type="radio" id="female" name="pernyataan[<?php echo $tampil->nomor?>]" value="0">
                  <label for="female">Tidak</label>-->
                </div>
                <br>
                <?php
                }
                ?>

              </div>

              <div class="col-md-12">
                <h3>Komentar</h3><br>
                <table id="example2" class="table table-bordered table-striped display">

                  <thead>

                  <tr>

                    <th width="10%">No</th>

                    <th>Komentar</th>

                  </thead>

                  <tbody>

                  <?php

                    $no=0;

                    foreach($tmp_pt as $tampil){

                    $no++;

                  ?>

                  <tr>

                    <td><?php echo $no;?></td>
                    <td><?php echo $tampil->komentar;?></td>
                  </tr>

                  <?php } ?>

                  </tbody>

                </table>
              </div>

              </div>
              </div>
              </div>
              <div class="box-footer">
                  <a href="<?php echo site_url('Sop_Controller/page/data_kuisioner/');?>" class="btn btn-md btn-danger"> Kembali</a>
                  
              </div>
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
         
          <!-- /.box -->

          
          <!-- /.box -->

          <!-- Input addon -->
          
          <!-- /.box -->
        </div>
      </div>
    </section>