<?php
$wajib = "";
$wajib=$this->uri->segment(4);
$open='Sop_Controller/simpan_pretest';
$username = $this->session->userdata('nama');
$us=$this->Sop_Model->qw("*","status_peserta","WHERE nim='$username'")->num_rows();
if($us!='0')
{
  redirect('Sop_Controller/page/data_kuisioner');
}
$data_pretest=$this->Sop_Model->qw("*","data_pernyataan_pre","ORDER BY nomor ASC")->result();
?>
<section class="content-header">
      <h1>
        Kuesioner
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Input</a></li>
        <li class="active">Kuesioner</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <form role="form" class="form-horizontal" action="<?php echo site_url($open);?>" method="POST" enctype="multipart/form-data">
              <div class="box-body">
                <?php
                if($wajib=='wajib'){
                  ?>
                  <div class="col-md-8" style="margin-left: -10px;">
                    <div class="alert alert-danger">
                      Kuisioner Wajib Diisi
                    </div>
                  </div>
                  <?php
                }
                ?>
                <div class="row">
                <div class="col-md-12">
                <div class="form-group">
                  <div class="col-sm-8">
                    Pilih satu / lebih pilihan dibawah ini sesuai dengan kondisi yang Anda rasakan dalam kurun waktu 1 bulan terakhir :
                    <table id="example2" class="table table-bordered table-striped display">
                      <tr>
                        <th>No</th>
                        <th>Pertanyaan</th>
                        <th></th>
                      </tr>
                      <?php
                      foreach($data_pretest as $pre){
                      ?>
                      <tr>
                        <td><?php echo $pre->nomor?></td>
                        <td><?php echo $pre->pernyataan?></td>
                        <td><input type="checkbox" id="pre<?php echo $pre->nomor?>" name="pre<?php echo $pre->nomor?>" value="<?php echo $pre->nomor?>"></td>
                      </tr>
                      <?php
                      }
                      ?>
                    </table>
                    <button type="submit" name="" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                  </div>
                </div>
                </div>
              </div>
              </div>
              </div>

              <!-- /.box-body -->
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
         
          <!-- /.box -->

          
          <!-- /.box -->

          <!-- Input addon -->
          
          <!-- /.box -->
        </div>
      </div>
    </section>