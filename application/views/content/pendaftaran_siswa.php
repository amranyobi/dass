<?php
          //$nip = "197709042010012001";
          $edit=$this->uri->segment(5);
          $admin=$this->uri->segment(6);
          if($edit=='1')
          {
            $hasil_ijin=$this->Sop_Model->qw("*","ijin_belajar","WHERE nip='$nip'"
        )->row_array();
            $tgij = ltrim($hasil_ijin['tanggal_ijazah'], '0');
            if($admin=='1')
            {
              $kembali = "Sop_Controller/page/lihat_pelaporan_status/".$nip;
            }else{
              $kembali = "Pengajuan_Controller/page/pelaporan_status/".$nip;
            }
          }else{
            $kembali = "Pengajuan_Controller/page/pelaporan";
          }
?>
<section class="content-header">
      <h1>
        Langkah 1 : Input Data Siswa
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Form</a></li>
        <li class="active">Input Data Siswa</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <form role="form" class="form-horizontal" action="<?php echo site_url($open);?>" method="POST" enctype="multipart/form-data">
              <input name="edit" value="<?php echo $edit?>" type="hidden">
              <input name="admin" value="<?php echo $admin?>" type="hidden">
              <input name="status" value="<?php if($edit=='1') { echo $hasil_ijin['status'];}?>" type="hidden">
              <div class="box-body">
                <div class="row">
                <div class="col-md-6">
                <div class="form-group">
                  <label class="col-sm-3">Nama</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="nama" autocomplete="off" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Asal Sekolah</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="sekolah" value="<?php if($edit=='1') { echo $hasil_ijin['sekolah'];}?>" autocomplete="off" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Kelas</label>
                  <div class="col-sm-2">
                    <select name="kelas" class="form-control">
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">No. Handphone</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" name="hp" autocomplete="off" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Email</label>
                  <div class="col-sm-9">
                    <input type="email" class="form-control" name="email" autocomplete="off">
                  </div>
                </div>
                </div>
              </div>
              </div>
              </div>

              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name="" class="btn btn-primary"><i class="fa fa-save"></i> Selanjutnya</button>
                <?php
                if($controller=='Pengajuan_Controller' && $edit=='')
                {
                  echo "";
                }else{
                  ?>
                  <a href="<?php echo site_url($kembali);?>" class="btn btn-danger"><i class="fa fa-close"></i> Batal</a>
                  <?php
                }
                ?>
              </div>
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
         
          <!-- /.box -->

          
          <!-- /.box -->

          <!-- Input addon -->
          
          <!-- /.box -->
        </div>
      </div>
    </section>