<?php
  $username=$this->session->userdata('nama');
  $level=$this->session->userdata('level');
  if($level=='1')
    $cont="Pengajuan_Controller";
  else
    $cont="Sop_Controller";
  $sukses=$this->uri->segment(4);
?>
<section class="content-header">
      <h1>
        Ubah Kata Sandi
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Form</a></li>
        <li class="active">Ubah Kata Sandi</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">

        <div class="col-md-6">
          <?php
          if($sukses=='sukses')
          {
          ?>
          <div class="callout callout-success">
          <h4>Kata Sandi Berhasil Diubah</h4>
          <p>
            Kata Sandi baru yang Anda masukkan sudah berhasil diubah. Untuk selanjutnya, gunakan kata sandi yang baru untuk masuk ke sistem.
          </p>
          </div>
          <?php
          }?>
          <div class="box box-primary">
            <form role="form" class="form-horizontal" action="<?php echo site_url($cont."/ubah_sandi");?>" method="POST" enctype="multipart/form-data">
              <input name="username" type="hidden" value="<?php echo $username?>">
              <div class="box-body">
                <div class="row">
                <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-5">Kata Sandi Baru</label>
                  <div class="col-sm-5">
                    <input name="password" id="txtPassword" type="password" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5">Konfirmasi Kata Sandi Baru</label>
                  <div class="col-sm-5">
                    <input id="txtConfirmPassword" name="ulangi_password" type="password" class="form-control">
                  </div>
                </div>
              </div>
              </div>
              </div>
              <div class="box-footer">
                <button type="submit" id="btnSubmit" class="btn btn-primary"><i class="fa fa-save"></i> Ubah</button>
              </div>
            </form>
          </div>
          </div>
        </div>
      </div>
    </section>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#btnSubmit").click(function () {
                var password = $("#txtPassword").val();
                var confirmPassword = $("#txtConfirmPassword").val();
                if (password != confirmPassword) {
                    alert("Isian Kata Sandi dan Konfirmasi Kata Sandi Tidak Sama.");
                    return false;
                }
                return true;
            });
        });
    </script>