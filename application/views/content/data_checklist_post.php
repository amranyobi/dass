<?php
  $id_jenis=$this->uri->segment(4);
  $val=$this->Sop_Model->qw("*","data_pernyataan","WHERE id_jenis='$id_jenis'")->result();
  $jenis_kues=$this->Sop_Model->qw("*","jenis_kuesioner","WHERE id_jenis='$id_jenis'")->row_array();
  $nim = $this->session->userdata('nama');
  $open='Sop_Controller/simpan_pernyataan_post';
  if($id_jenis=='1')
    $istilah = "Depresi";
  elseif($id_jenis=='2')
    $istilah = "Kecemasan";
  else
    $istilah = "Stress";
?>
<section class="content-header">
      <h1>
        Pengisian Post Kuesioner (<?php echo $istilah?>)
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Form</a></li>
        <li class="active">Pengisian Post Kuesioner</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">

        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <form role="form" class="form-horizontal" action="<?php echo site_url($open);?>" method="POST" enctype="multipart/form-data">
              <input name="jenis_kuesioner" value="<?php echo $id_jenis?>" type="hidden">
              <input name="nim" value="<?php echo $nim?>" type="hidden">
              <div class="box-body">
                <div class="row">
                <div class="col-md-12">
                <?php

                  $no=0;

                  foreach($val as $tampil){

                  $no++;

                ?>
                <input name="no_pernyataan[]" value="<?php echo $tampil->nomor?>" type="hidden">
                <p><?php echo $tampil->nomor?>. <?php echo $tampil->pernyataan?></p>
                <div style="margin-left: 15px">
                  <?php
                  $jwb=$this->Sop_Model->qw("*","jawaban_kuesioner","WHERE id_jenis='$id_jenis' ORDER BY nilai_jawaban ASC")->result();
                  foreach($jwb as $jw){
                    ?>
                    <input type="radio" id="<?php echo $jw->nilai_jawaban?>" name="pernyataan[<?php echo $tampil->nomor?>]" value="<?php echo $jw->nilai_jawaban?>" required>
                    <label for="male"><?php echo $jw->jawaban?></label><br>
                    <?php
                  }
                  ?>
                  <!--
                  <input type="radio" id="male" name="pernyataan[<?php echo $tampil->nomor?>]" value="1" required>
                  <label for="male">Ya</label><br>
                  <input type="radio" id="female" name="pernyataan[<?php echo $tampil->nomor?>]" value="0">
                  <label for="female">Tidak</label>-->
                </div>
                <br>
                <?php
                }
                ?>
              </div>
              </div>
              </div>
              </div>
              <div class="box-footer">
                  <button type="submit" name="" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
         
          <!-- /.box -->

          
          <!-- /.box -->

          <!-- Input addon -->
          
          <!-- /.box -->
        </div>
      </div>
    </section>