<section class="content-header">
      <h1>
        Langkah 1 : Input Data Diri
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Form</a></li>
        <li class="active">Input Data Diri</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <form role="form" class="form-horizontal" action="<?php echo site_url($open);?>" method="POST" enctype="multipart/form-data">
              <div class="box-body">
                <div class="row">
                <div class="col-md-6">
                <div class="form-group">
                  <label class="col-sm-3">Nama Lengkap (beserta gelar)</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="nama" autocomplete="off" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Email</label>
                  <div class="col-sm-9">
                    <input type="email" class="form-control" name="email" autocomplete="off" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Status</label>
                  <div class="col-sm-4">
                    <select name="status" class="form-control">
                      <option value="1">Mahasiswa Sarjana</option>
                      <option value="2">Mahasiswa Profesi</option>
                      <option value="3">Dosen Sarjana</option>
                      <option value="4">Dosen Profesi</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">NIM / NIK</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="nimnik" autocomplete="off" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Angkatan (Tahun)</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" name="angkatan" autocomplete="off"> <div style="font-size: 12px"><font color="red">*Untuk Mahasiswa</font></div>
                  </div>
                </div>
                </div>
              </div>
              </div>
              </div>

              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name="" class="btn btn-primary"><i class="fa fa-save"></i> Selanjutnya</button>
                <a href="<?php echo site_url('');?>" class="btn btn-danger">Batal</a>
              </div>
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
         
          <!-- /.box -->

          
          <!-- /.box -->

          <!-- Input addon -->
          
          <!-- /.box -->
        </div>
      </div>
    </section>