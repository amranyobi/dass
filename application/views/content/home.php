<section class="content-header">
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <?php
        if($_SESSION['level']=='tendik')
        {
          ?>
          <div class="col-lg-3 col-xs-6">
            <!-- small box 
            <div class="small-box bg-aqua">
              <div class="inner">
                <?php
                  $us=$this->Sop_Model->qw2("*","data_pembimbing","")->num_rows();
                ?>
                <h3><?php echo $us;?></h3>

                <p>Total Pembimbing</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="<?php echo site_url("Sop_Controller/page/lihat_pembimbing"); ?>" class="small-box-footer">Info Lebih Lanjut <i class="fa fa-arrow-circle-right"></i></a>
            </div>-->
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
              <div class="inner">
                <?php
                  $ujian=$this->Sop_Model->qw("*","data_ujian","")->num_rows();
                ?>
                <h3><?php echo $ujian;?></h3>

                <p>Data Ujian</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="<?php echo site_url("Sop_Controller/page/lihat_ujian"); ?>" class="small-box-footer">Info Lebih Lanjut <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <?php
        }
        elseif($_SESSION['level']=='mahasiswa')
        {
          ?>
          <div class="col-lg-3 col-xs-6">
          </div>
          <?php
        }
        ?>
        
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->

    </section>