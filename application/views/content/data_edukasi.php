<?php
$username = $this->session->userdata('nama');
$data=$this->Sop_Model->qw("*","status_peserta","WHERE nim='$username'")->row_array();
$pre1 = $data['pre1'];
$pre2 = $data['pre2'];
$pre3 = $data['pre3'];
$query = "";
if ($pre1 == 1) {
    $query .= " OR tipe = 1";
}

if ($pre2 == 1) {
    $query .= " OR tipe = 2";
}

if ($pre3 == 1) {
    $query .= " OR tipe = 3";
}
$us=$this->Sop_Model->qw("*","data_edukasi","WHERE tipe=4 $query ORDER BY id ASC")->result(); 

?>
<section class="content-header">
      <h1>
        Data Edukasi Kesehatan Jiwa
      </h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li><a href="#">Data</a></li>

        <li class="active">Data Edukasi Kesehatan Jiwa</li>

      </ol>

</section>



   <section class="content">

      <div class="row">

        <div class="col-xs-12">

          <div class="box box-primary">

            <!-- /.box-header 

            <div class="box-header" style="margin-top: 20px;">
              <div align="right" style="margin-right: 20px">
                <a href="" class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-file-excel-o"></i> Export</a>
              </div>
            </div>-->

            <div class="box-body">

              <table id="example2" class="table table-bordered table-striped display">

                <thead>

                <tr>

                  <th>Tipe</th>

                  <th>Judul</th>
                  <th>Deskripsi</th>
                  <th>Video Edukasi</th>
                </tr>

                </thead>

                <tbody>

                <?php

                  $no=0;

                  foreach($us as $tampil){

                  $no++;

                  if($tampil->tipe=='4')
                  {
                    ?>
                    <tr>

                      <td><?php 
                      if($tampil->tipe=='1')
                        echo "Artikel Depresi";
                      elseif($tampil->tipe=='2')
                        echo "Artikel Kecemasan";
                      elseif($tampil->tipe=='3')
                        echo "Artikel Stress";
                      elseif($tampil->tipe=='4')
                        echo "Artikel Umum";
                      ?></td>
                      <td><?php echo $tampil->judul;?></td>
                      <td><?php echo $tampil->deskripsi;?></td>
                      <!-- <td><a target="_blank" href="<?php echo $tampil->link?>" class="btn btn-sm btn-success"><i class="fa fa-play"></i> Lihat</a></td> -->
                      <td><iframe width="350" height="250" src="<?php echo $tampil->link?>" title="Seberapa Penting Kesehatan Mental Untuk Kita?" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe></td>

                    </tr>
                    <?php
                  }

                  if($data['pre1']=='1' && $tampil->tipe=='1')
                  {
                    ?>
                    <tr>

                      <td><?php 
                      if($tampil->tipe=='1')
                        echo "Artikel Depresi";
                      elseif($tampil->tipe=='2')
                        echo "Artikel Kecemasan";
                      elseif($tampil->tipe=='3')
                        echo "Artikel Stress";
                      elseif($tampil->tipe=='4')
                        echo "Artikel Umum";
                      ?></td>
                      <td><?php echo $tampil->judul;?></td>
                      <td><?php echo $tampil->deskripsi;?></td>
                      <!-- <td><a target="_blank" href="<?php echo $tampil->link?>" class="btn btn-sm btn-success"><i class="fa fa-play"></i> Lihat</a></td> -->
                      <td><iframe width="350" height="250" src="<?php echo $tampil->link?>" title="Seberapa Penting Kesehatan Mental Untuk Kita?" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe></td>

                    </tr>
                    <?php
                  }

                  if($data['pre2']=='1' && $tampil->tipe=='2')
                  {
                    ?>
                    <tr>

                      <td><?php 
                      if($tampil->tipe=='1')
                        echo "Artikel Depresi";
                      elseif($tampil->tipe=='2')
                        echo "Artikel Kecemasan";
                      elseif($tampil->tipe=='3')
                        echo "Artikel Stress";
                      elseif($tampil->tipe=='4')
                        echo "Artikel Umum";
                      ?></td>
                      <td><?php echo $tampil->judul;?></td>
                      <td><?php echo $tampil->deskripsi;?></td>
                      <!-- <td><a target="_blank" href="<?php echo $tampil->link?>" class="btn btn-sm btn-success"><i class="fa fa-play"></i> Lihat</a></td> -->
                      <td><iframe width="350" height="250" src="<?php echo $tampil->link?>" title="Seberapa Penting Kesehatan Mental Untuk Kita?" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe></td>

                    </tr>
                    <?php
                  }

                  if($data['pre3']=='1' && $tampil->tipe=='3')
                  {
                    ?>
                    <tr>

                      <td><?php 
                      if($tampil->tipe=='1')
                        echo "Artikel Depresi";
                      elseif($tampil->tipe=='2')
                        echo "Artikel Kecemasan";
                      elseif($tampil->tipe=='3')
                        echo "Artikel Stress";?></td>
                      <td><?php echo $tampil->judul;?></td>
                      <td><?php echo $tampil->deskripsi;?></td>
                      <!-- <td><a target="_blank" href="<?php echo $tampil->link?>" class="btn btn-sm btn-success"><i class="fa fa-play"></i> Lihat</a></td> -->
                      <td><iframe width="350" height="250" src="<?php echo $tampil->link?>" title="Seberapa Penting Kesehatan Mental Untuk Kita?" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe></td>

                    </tr>
                    <?php
                  }

                ?>

                

                <?php } ?>

                </tbody>

              </table>

              <font color="red">Ket : Untuk hasil kuesioner <b>Sedang</b> dan <b>Berat</b> ditambahkan catatan rekomendasi Segera Konsultasi ke Guru BK</font>

            </div>

            <!-- /.box-body -->

          </div>

          <!-- /.box -->

        </div>

        <!-- /.col -->

      </div>

      <!-- /.row -->

    </section>