<?php
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");

// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=data_peserta.xls");
?>
<section class="content-header">

      <h1>
        Data Peserta
      </h1>
</section>



   <section class="content">

      <div class="row">

        <div class="col-xs-12">

          <div class="box box-primary">

            <div class="box-body">

              <table id="example2" class="table table-bordered table-striped display nowrap" border="1">

                <thead>

                <tr style="background-color: #CCCCCC">

                  <th>No</th>
                  <th>Nama</th>
                  <th>Sekolah</th>
                  <th>Kelas</th>
                  <th>Handphone</th>
                  <th>Email</th>
                  <th>Waktu Input</th>

                </tr>

                </thead>

                <tbody>

                <?php

                  $no=0;

                  foreach($tmp_pt as $tampil){

                  $no++;

                ?>

                <tr>

                  <td><?php echo $no;?></td>
                  <td><?php echo $tampil->nama;?></td>
                  <td><?php echo $tampil->sekolah;?></td>
                  <td><?php echo $tampil->kelas;?></td>
                  <td>'<?php echo $tampil->hp;?></td>
                  <td><?php echo $tampil->email;?></td>
                  <td><?php echo date("d-m-Y", strtotime($tampil->waktu_input));?></td>

                </tr>

                <?php } ?>

                </tbody>

              </table>

            </div>

            <!-- /.box-body -->

          </div>

          <!-- /.box -->

        </div>

        <!-- /.col -->

      </div>

      <!-- /.row -->

    </section>