<?php
class Sop_Controller extends CI_Controller {
	function __construct(){
		parent:: __construct();
		if ($this->session->userdata('nama')=="") {
	 		 if($this->session->userdata('level')!='1'){
	 				 if($this->session->userdata('level')!='5'){
	 				 	redirect('LoginAdmin_Controller');
	 				 }
	 		 }
	 	 }
		$this->load->model('Sop_Model');
		// $this->load->library('pdf');
		setlocale (LC_TIME, 'id_ID');
	}
	function page(){
		$page=$this->uri->segment(3);
		$data['page']=$page;
		if($page=="user"){
			$data['tmp_pt']=$this->Sop_Model->qw("*","user","")->result();
		}elseif($page=="pelaksana"){
			$data['tmp_pt']=$this->Sop_Model->qw("*","pelaksana","")->result();
		}elseif($page=="data_peserta"){
			$data['tmp_pt']=$this->Sop_Model->qw("*","data_peserta","ORDER BY id DESC")->result();
		}elseif($page=="lihat_pelaporan_status"){
			$nip=$this->uri->segment(4);
			$data['data_pegawai'] = $this->get_pegawai($nip);
		}elseif($page=="pelaporan_siap"){
			$data['open']='Pengajuan_Controller/simpan_pelaporan';
		}elseif($page=="lihat_pelaporan_suket"){
			$nip=$this->uri->segment(4);
			$data['nip'] = $nip;
			$data['data_pegawai'] = $this->get_pegawai($nip);
			$data['open']='Sop_Controller/simpan_suket';
		}elseif($page=="lihat_pelaporan_verifikasi"){
			$data['open']='Sop_Controller/simpan_verifikasi';
		}elseif($page=="lihat_pengguna"){
			$username = $this->session->userdata('nama');
			if($username=='superadmin' || $username=='superfatma')
            {
            	$data['tmp_pt']=$this->Sop_Model->qw("*","user","WHERE username!='superphd' AND username!='superadmin' AND username!='superfatma' ORDER BY username ASC")->result();
            }else if($username=='superphd')
            {
            	$data['tmp_pt']=$this->Sop_Model->qw("*","user","WHERE username!='superphd' ORDER BY username ASC")->result();
            }else{
            	$data['tmp_pt']=$this->Sop_Model->qw("*","user","WHERE username!='admin' AND username!='cepiring' AND username!='brangsong' AND username!='kaliwungu' AND username!='superadmin' AND username!='superphd' AND username!='superfatma' ORDER BY username ASC")->result();
            }
            $data['username'] = $username;
			
		}elseif($page=="lihat_pejabat"){
			$data['tmp_pt']=$this->Sop_Model->qw("*","pejabat","ORDER BY id ASC")->result();
		}
		$this->load->view('index',$data);
	}

	function cetak_laporan(){
		$data['tmp_pt']=$this->Sop_Model->qw("*","data_peserta","ORDER BY id DESC")->result();
		$this->load->view('export/cetak_laporan',$data);
	}

	function simpan_pernyataan(){
			$nim = $this->input->post('nim');
    	$jenis_kuesioner = $this->input->post('jenis_kuesioner');
    	$next_kuisioner = $jenis_kuesioner + 1;
		//$halaman = $this->input->post('halaman');
		//$next = $halaman+1;

		foreach ($this->input->post('no_pernyataan') as $key => $value) {
			
			$jawaban = $this->input->post('pernyataan')[$value];
			if($key=='0')
			{
				$ary=array(
					'nim'			=>$nim,
					'jenis_kuesioner'     =>$jenis_kuesioner,
	        'id_pernyataan'		=>$value,
					'jawaban'			=>$jawaban,
				);
			}else{
				$ary=array(
					'nim'			=>$nim,
					'jenis_kuesioner'     =>$jenis_kuesioner,
	        'id_pernyataan'		=>$value,
					'jawaban'			=>$jawaban,
				);
			}
			$total_jawaban = $total_jawaban + $jawaban;
			
			$this->Sop_Model->simpan_jawaban('data_jawaban',$ary);
		}
				if($jenis_kuesioner=='1')
					$tab = "sebelum1";
				elseif($jenis_kuesioner=='2')
					$tab = "sebelum2";
				elseif($jenis_kuesioner=='3')
					$tab = "sebelum3";


				$time = date("Y-m-d H:i:s");
				$ary2=array(
					'tanggal_kuesioner_pre' =>$time,
					'status'			=>2,
					$tab			=>$total_jawaban,
				);

				$this->Sop_Model->edit_status('status_peserta',$nim,$ary2);
		//if($halaman=='8')
		//	redirect('Pendaftaran_Controller/page/hasil_survey/'.$id_siswa.'');
		//else
		
		redirect('Sop_Controller/page/data_kuisioner/');
		
	}

	function simpan_pernyataan_post(){
			$nim = $this->input->post('nim');
    	$jenis_kuesioner = $this->input->post('jenis_kuesioner');
    	$next_kuisioner = $jenis_kuesioner + 1;
		//$halaman = $this->input->post('halaman');
		//$next = $halaman+1;

		foreach ($this->input->post('no_pernyataan') as $key => $value) {
			
			$jawaban = $this->input->post('pernyataan')[$value];
			if($key=='0')
			{
				$ary=array(
					'nim'			=>$nim,
					'jenis_kuesioner'     =>$jenis_kuesioner,
	        'id_pernyataan'		=>$value,
					'jawaban'			=>$jawaban,
				);
			}else{
				$ary=array(
					'nim'			=>$nim,
					'jenis_kuesioner'     =>$jenis_kuesioner,
	        'id_pernyataan'		=>$value,
					'jawaban'			=>$jawaban,
				);
			}
			$total_jawaban = $total_jawaban + $jawaban;
			
			$this->Sop_Model->simpan_jawaban('data_jawaban_post',$ary);
		}
				if($jenis_kuesioner=='1')
					$tab = "sesudah1";
				elseif($jenis_kuesioner=='2')
					$tab = "sesudah2";
				elseif($jenis_kuesioner=='3')
					$tab = "sesudah3";


				$time = date("Y-m-d H:i:s");
				$ary2=array(
					'tanggal_kuesioner_post' =>$time,
					'status'			=>3,
					$tab			=>$total_jawaban,
				);

				$this->Sop_Model->edit_status('status_peserta',$nim,$ary2);
		//if($halaman=='8')
		//	redirect('Pendaftaran_Controller/page/hasil_survey/'.$id_siswa.'');
		//else
		
		redirect('Sop_Controller/page/data_kuisioner_post/');
		
	}

	function get_pegawai($nip){
		  $url = 'https://simpeg.kendalkab.go.id/v17/api/pegawaidetail/'.$nip;
          $curl = curl_init();
          curl_setopt_array($curl, array(
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 60,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              CURLOPT_HTTPHEADER => array(
                  "content-type: application/x-www-form-urlencoded"
              ),
              CURLOPT_URL => $url
          ));

          $response = curl_exec($curl);
          $curl_errno = curl_errno($curl);
          $curl_error = curl_error($curl);
          curl_close($curl);
          $svcpegawai = json_decode($response, true);
          $coba = $svcpegawai['pegawaidetail'][0];
          return $coba;
	}

	function lokasi_file($jenis_file,$id_ijin)
	{
		$getfile=$this->Sop_Model->qw("*","file_peserta","WHERE id_ijin='$id_ijin' AND jenis_file='$jenis_file'")->row_array();
		return $getfile;
	}

	function pelaporan_verifikasi($id,$nip){
		$tanggal=date("Y-m-d");
		$ary=array(
			'status'				=>1,
			'tanggal_verifikasi'	=>$tanggal
		);
		$this->Sop_Model->edit_status('ijin_belajar',$id,$ary);
		redirect('Sop_Controller/page/lihat_pelaporan_status/'.$nip);
	}

	function simpan_pretest(){
		$pre1 = $this->input->post('pre1');
		$pre2 = $this->input->post('pre2');
		$pre3 = $this->input->post('pre3');

		if($pre1==''&&$pre2==''&&$pre3==''){
			redirect('Sop_Controller/page/pretest/wajib');
		}

		$nim = $this->session->userdata('nama');
		if(isset($pre1))
			$pre1 = 1;
		else
			$pre1 = 0;

		if(isset($pre2))
			$pre2 = 1;
		else
			$pre2 = 0;

		if(isset($pre3))
			$pre3 = 1;
		else
			$pre3 = 0;
		$time = date("Y-m-d H:i:s");
		$ary=array(
			'nim'	=>$nim,
			'tanggal_pretest'	=>$time,
			'status'	=>1,
			'pre1'		=>$pre1,
			'pre2'		=>$pre2,
			'pre3'		=>$pre3,
		);
		$this->Sop_Model->simpan_pretest('status_peserta',$ary);
		redirect('Sop_Controller/page/data_kuisioner');
	}

	function simpan_user(){
		$username = $this->input->post('username');
		$hitung=$this->Sop_Model->qw('*','user',"WHERE username='$username'")->num_rows();
		if($hitung!='0')
		{
			redirect('Sop_Controller/page/user/');
		}
		$ary=array(
			'id_user'	=>$this->input->post('id_user'),
			'username'	=>$this->input->post('username'),
			'password'	=>md5($this->input->post('password')),
			'level'		=>$this->input->post('level')
		);
		$this->Sop_Model->simpan_user('user',$ary);
		redirect('Sop_Controller/page/user');
	}

	function simpan_verifikasi(){
		$nip = $this->input->post('nip');
		$jenis_file = $this->input->post('jenis_file');
		$id_ijin = $this->input->post('id_ijin');
		$fileaja=$this->Sop_Model->qw('*','file_peserta',"WHERE id_ijin='$id_ijin' AND jenis_file='$jenis_file'")->row_array();
		$id_file=$fileaja['id'];
		$status = $this->input->post('status');
		$ary=array(
			'status'	=>$status,
			'keterangan'		=>$this->input->post('keterangan'),
			'revisi' =>0
		);
		$this->Sop_Model->simpan_verifikasi('file_peserta',$id_file,$ary);
		if($status=='1')
		{
			$username = $this->session->userdata('nama');
			//verifikator
			$ver=array(
			'nip'			=>$nip,
			'verifikator'	=>$username
			);

			$hitung=$this->Sop_Model->qw('*','plot_verifikasi',"WHERE nip='$nip'")->num_rows();
			if($hitung=='0')
			{
				$this->Sop_Model->simpan_verifikator('plot_verifikasi',$ver);
			}
		}
		redirect('Sop_Controller/page/lihat_pelaporan_status/'.$nip);
	}

	function simpan_suket(){
		$nip = $this->input->post('nip');
		$tanggal_input = date("Y-m-d");

		$id_ijin = $this->input->post('id_ijin');
		$hitung=$this->Sop_Model->qw("*","surat_keterangan","WHERE id_ijin='$id_ijin'"
        )->num_rows();
		
		$ary=array(
			'id_ijin'	=>$id_ijin,
			'tanggal_input'	=>$tanggal_input,
			'tanggal_surat'	=>$this->input->post('tanggal_surat'),
			'nomor'	=>$this->input->post('nomor'),
			'nomor_keuangan'	=>$this->input->post('nomor_keuangan'),
			'keterangan'	=>$this->input->post('keterangan'),
			'jabatan_pengesah_1'	=>$this->input->post('jabatan_pengesah_1'),
			'jabatan_pengesah_2'	=>$this->input->post('jabatan_pengesah_2'),
			'perihal'	=>$this->input->post('perihal')
			);

		if($hitung=='1')
		{
			$this->Sop_Model->edit_suket('surat_keterangan',$id_ijin,$ary);
		}else{
			$this->Sop_Model->simpan_suket('surat_keterangan',$ary);
		}
		
		redirect('Sop_Controller/page/lihat_pelaporan_status/'.$nip);
	}
	
	function edit_user(){
		$id=$this->input->post('id_user');
		$password=md5($this->input->post('password'));
		$ary=array(
			'id_user'	=>$this->input->post('id_user'),
			'username'	=>$this->input->post('username'),
			'password'	=>$password,
			'level'		=>$this->input->post('level')
		);
		$this->Sop_Model->edit_user('user',$id,$ary);
		redirect('Sop_Controller/page/user');
	}

	function ubah_sandi(){
		$username=$this->input->post('username');
		$password=md5($this->input->post('password'));
		$ary=array(
			'password'	=>$password
		);
		$this->Sop_Model->reset_password('user',$username,$ary);
		redirect('Sop_Controller/page/ubah_sandi/sukses');
	}

	function reset_password($nip){
		$password=md5("bkpp2019");
		$ary=array(
			'password'	=>$password
		);
		$this->Sop_Model->reset_password('user',$nip,$ary);
		redirect('Sop_Controller/page/lihat_pengguna/'.$nip);
	}

	function ubah_pejabat(){
		$id=$this->input->post('id');
		$pejabat_baru = $this->input->post('pejabat_baru');
		$pjb = $this->get_pegawai($pejabat_baru);
		if($pjb['status']=='false')
		{
			redirect('Sop_Controller/page/ubah_pejabat/'.$id.'/error');
		}else{
			$ary=array(
			'nip'	=>$pejabat_baru
			);
			$this->Sop_Model->ubah_pejabat('pejabat',$id,$ary);
			redirect('Sop_Controller/page/lihat_pejabat/'.$id);
		}
		
	}
	
	function hapus_user($id){
		$this->Sop_Model->hapus_user('user',$id);
		redirect('Sop_Controller/page/user');
	}

	function masainput($tanggal){
		//buat tanggal
		$array_months = ["empty", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
		$masainput = date('d', strtotime($tanggal))." ".$array_months[date("n", strtotime($tanggal))]." ".date('Y', strtotime($tanggal));
		return $masainput;
	}

	function get_bulan($bulan){
		//buat bulan
		$input = ltrim($bulan, '0');
		$array_months = ["empty", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
		$masainput = $array_months[$input];
		return $masainput;
	}
	
	function cetak_suket($nip){
		//$hasil = $this->get_pegawai($nip);
		$this->load->library('ciqrcode'); //pemanggilan library QR CODE
		//setting QR
		$config['cacheable']    = true; //boolean, the default is true
        $config['cachedir']     = './assets/'; //string, the default is application/cache/
        $config['errorlog']     = './assets/'; //string, the default is application/logs/
        $config['imagedir']     = './assets/qr/'; //direktori penyimpanan qr code
        $config['quality']      = true; //boolean, the default is true
        $config['size']         = '1024'; //interger, the default is 1024
        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);
 
        $image_name=$nip.'.png'; //buat name dari qr code sesuai dengan nim
 
        $params['data'] = site_url('DigitalSign_Controller/page/digitalsign/'.$nip); //data yang akan di jadikan QR CODE
        $params['level'] = 'H'; //H=High
        $params['size'] = 10;
        $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
        $this->ciqrcode->generate($params);
        $image1 = "./assets/qr/".$nip.".png";

		$ijin=$this->Sop_Model->qw("ijin_belajar.*, jenjang.jenjang AS nama_jenjang","ijin_belajar, jenjang","WHERE ijin_belajar.nip='$nip' AND ijin_belajar.jenjang=jenjang.id")->row_array();
		if($ijin['jenjang']=='10' || $ijin['jenjang']=='11')
		{
			$ttd = "Sekretaris Daerah";
			$pjb = "sekda";
		}else{
			$ttd = "Kepala Kepegawaian, Pendidikan dan Pelatihan";
			$pjb = "bkpp";
		}
		$ambil_pejabat=$this->Sop_Model->qw("*","pejabat","WHERE keterangan='$pjb'")->row_array();
		$nip_pejabat = $ambil_pejabat['nip'];

		//get_data
		$pjb = $this->get_pegawai($nip_pejabat);
		$pelapor = $this->get_pegawai($nip);

		$suket=$this->Sop_Model->qw("*","surat_keterangan","WHERE id_ijin='$ijin[id]'")->row_array();

        $pdf = new FPDF('l','mm','A4');
        // membuat halaman baru
        $pdf->AddPage('P');
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','U',14);
        // mencetak string 
        $pdf->Cell(10,40,'',0,1);
        $pdf->Cell(190,7,'SURAT KETERANGAN',0,1,'C');
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(190,7,'Nomor : '.$suket['nomor'],0,1,'C');
        $pdf->SetFont('Arial','B',11);
        $pdf->Cell(190,5,'',0,1);
        $pdf->SetFont('Arial','',11);
        $pdf->MultiCell(190,7,'Berdasarkan surat '.$suket['nomor_keuangan'].' tanggal '.$this->masainput($suket['tanggal_surat']).' perihal permohonan '.$suket['perihal'].', menerangkan bahwa :',0,1);
        $pdf->Cell(190,3,'',0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(45,7,'Nama',0,0);
        $pdf->Cell(70,7,': '.$pelapor['nama_pejabat'],0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->Cell(45,7,'NIP',0,0);
        $pdf->Cell(70,7,': '.$nip,0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->Cell(45,7,'Pangkat/Gol. Ruang',0,0);
        $pdf->Cell(70,7,': '.$pelapor['pangkat'].', '.$pelapor['golongan'],0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->Cell(45,7,'Jabatan',0,0);
        $pdf->Cell(70,7,': '.$pelapor['jabatan'],0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->Cell(45,7,'Unit Kerja',0,0);
        $pdf->Cell(2,7,':',0,0);
        $pdf->MultiCell(130,7,$pelapor['nama_induk'],0,1);
        $pdf->Cell(190,3,'',0,1);
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(40,7,'Telah melaksanakan pendidikan pada :',0,1);
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,3,'',0,1);
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(5,7,'',0,0);
        $pdf->Cell(45,7,'Sekolah / Universitas',0,0);
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(100,7,': '.$ijin['sekolah'],0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(45,7,'Jenjang Pendidikan',0,0);
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(100,7,': '.$ijin['nama_jenjang'],0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(45,7,'Program Studi',0,0);
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(100,7,': '.$ijin['prodi'],0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(45,7,'Gelar Akademik',0,0);
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(100,7,': '.$ijin['gelar_akademik'],0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(45,7,'Nomor Ijazah',0,0);
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(100,7,': '.$ijin['nomor_ijazah'],0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(45,7,'Tanggal Ijazah',0,0);
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(100,7,': '.$ijin['tanggal_ijazah']." ".$this->get_bulan($ijin['bulan_ijazah'])." ".$ijin['tahun_ijazah'],0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->Cell(10,3,'',0,1);
        $pdf->Cell(190,7,'Yang diperoleh sebelum diserahkannya '.$suket['keterangan'],0,1);
        $pdf->Cell(190,7,'Demikian Surat Keterangan ini dibuat untuk dapat dipergunakan sebagaimana mestinya',0,1);
        $pdf->Cell(10,3,'',0,1);
        $pdf->Cell(100,7,'',0,0);
        $pdf->Cell(90,7,'Kendal, '.$this->masainput($ijin['tanggal_input']),0,1,'C');
        $pdf->Cell(100,7,'',0,0);
        $pdf->Cell(90,7,'A.n BUPATI KENDAL',0,1,'C');
        $pdf->Cell(100,7,'',0,0);
        $pdf->Image($image1, 20, $pdf->GetY(), 33.78);
        $pdf->Cell(90,7,$suket['jabatan_pengesah_1'].' '.$ttd,0,1,'C');
        if(isset($suket['jabatan_pengesah_2']))
        {
        	$pdf->Cell(100,7,'',0,0);
        	$pdf->Cell(90,7,$suket['jabatan_pengesah_2'],0,1,'C');
        }
        $pdf->Cell(10,25,'',0,1);
        $pdf->Cell(100,5,'',0,0);
        $pdf->SetFont('Arial','U',11);
        $pdf->MultiCell(90,5,$pjb['nama_pejabat'], 0, 'C' , false);
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(100,5,'',0,0);
        $pdf->Cell(90,5,$pjb['pangkat'],0,1,'C');
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(100,5,'',0,0);
        $pdf->Cell(90,5,'NIP.'.$nip_pejabat,0,0,'C');
        $pdf->Output('','Surat_Keterangan_'.$nip.'.pdf');
    }

    function logout(){
		$this->session->sess_destroy();
		redirect('LoginAdmin_Controller');
	}
}
