<?php
class LoginAdmin_Controller extends CI_Controller {
	function __construct(){
		parent:: __construct();
		$this->load->model('Login_Model');
	}
	function index(){
		$this->load->view('loginadmin');
	}
	function aksi_login(){
			$username=$this->input->post('nip');
			$password=$this->input->post('password');
			$where=array(
					'username' =>$username,
					//'password' =>md5($password)
					'password' =>md5('oraora'.$password)
				);
			$cek=$this->Login_Model->cek_login('user',$where)->num_rows();
			$query=$this->db->where('username',$username)->get('user');
			$row=$query->row();
			$level=$row->level;
			if($cek > 0){
				$data_session=array(
					'nama'	=>$username,
					'id_user'	=>$row->id,
					'status' => 'login',
					'level'=>$level
					);
				$this->session->set_userdata($data_session);
				if($level=='5')
					redirect('Sop_Controller/page/data_peserta');
				else if($level=='mahasiswa')
					redirect('Sop_Controller/page/home');
				else if($level=='admprodi')
					redirect('Sop_Controller/page/home');
			}else{
				redirect('LoginAdmin_Controller/index/error');
			}
		}
	function logout(){
		$this->session->sess_destroy();
		redirect('LoginAdmin_Controller');
	}
}
