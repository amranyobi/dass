<?php
class Tamu_Controller extends CI_Controller {
	function __construct(){
		parent:: __construct();
		$this->load->model('Sop_Model');
		$this->load->library('pdf');
		$this->load->helper(array('form', 'url'));
	}
	function page(){
		$page=$this->uri->segment(3);
		$data['page']=$page;
		if($page=="user"){
			$data['tmp_pt']=$this->Sop_Model->qw("*","user","")->result();
		}elseif($page=="persetujuan"){
			$yy=$this->uri->segment(4);
			$data['tmp_pt']=$this->Sop_Model->qw("*","persetujuan","WHERE id_sop= '$yy'")->result();
			$data['sop']=$this->Sop_Model->qw("*","sop","WHERE no_sop= '$yy'");
		}elseif($page=="identitas_aplikasi"){
			$data['tmp_pt']=$this->Sop_Model->qw("*","identitas_aplikasi","")->result();
		}elseif($page=="pelaporan"){
			$data['open']='Tamu_Controller/page/pelaporan_siap';
		}elseif($page=="pelaporan_cek"){
			$data['open']='Tamu_Controller/page/pelaporan_status';
		}elseif($page=="pelaporan_siap"){
			$data['open']='Tamu_Controller/simpan_pelaporan';
		}elseif($page=="pelaporan_status"){
			$nip=$this->input->post('nip');
			if(!isset($nip))
			{
				$nip=$this->uri->segment(4);
			}
			$data['data_pegawai'] = $this->get_pegawai($nip);
		}elseif($page=="pelaporan_upload"){
			$data['nip']=$this->uri->segment(4);
			$data['id_ijin']=$this->uri->segment(5);
			$data['jenis_file']=$this->uri->segment(6);
			$data['open']='Pengajuan_Controller/pelaporan_unggah';
		}elseif($page=='pelaporan_tersimpan'){
			$nip=$this->uri->segment(4);
			$data['data_pegawai'] = $this->get_pegawai($nip);
		}elseif($page=="pelaporan_user"){
			$data['open']='Tamu_Controller/page/daftar_pengguna';
		}elseif($page=="daftar_pengguna"){
			$nip=$this->input->post('nip');
			$data['data_pegawai'] = $this->get_pegawai($nip);
		}elseif($page=="pengguna_sukses"){
			$nip=$this->uri->segment(4);
			$data['data_pegawai'] = $this->get_pegawai($nip);
		}
		$this->load->view('tamu',$data);
	}


	function detail($xx){
		$no_sop=$this->uri->segment(4);
		if(empty($no_sop)){
         $data['dt']=$this->Sop_Model->qw("detail_pelaksana.id_pelaksana,detail_pelaksana.no_sop,pelaksana.nama_pelaksana","pelaksana","inner join detail_pelaksana on detail_pelaksana.id_pelaksana=pelaksana.id_pelaksana WHERE detail_pelaksana.no_sop='$xx'")->result();
		}else{
			$data['dt']=$this->Sop_Model->qw("detail_pelaksana.id_pelaksana,detail_pelaksana.no_sop,pelaksana.nama_pelaksana","pelaksana","inner join detail_pelaksana on detail_pelaksana.id_pelaksana=pelaksana.id_pelaksana WHERE detail_pelaksana.no_sop='$no_sop'")->result();
		}
        $this->load->view('content/detail_pelaksana.php',$data);
	}

	function get_pegawai($nip){
		  $url = 'https://simpeg.kendalkab.go.id/v17/api/pegawaidetail/'.$nip;
          $curl = curl_init();
          curl_setopt_array($curl, array(
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 60,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              CURLOPT_HTTPHEADER => array(
                  "content-type: application/x-www-form-urlencoded"
              ),
              CURLOPT_URL => $url
          ));

          $response = curl_exec($curl);
          $curl_errno = curl_errno($curl);
          $curl_error = curl_error($curl);
          curl_close($curl);
          $svcpegawai = json_decode($response, true);
          $coba = $svcpegawai['pegawaidetail'][0];
          return $coba;
	}

	function lokasi_file($jenis_file,$id_ijin)
	{
		$getfile=$this->Sop_Model->qw("*","file_peserta","WHERE id_ijin='$id_ijin' AND jenis_file='$jenis_file'")->row_array();
		return $getfile;
	}

	function pelaporan_unggah()
	{
		// setting konfigurasi upload
		date_default_timezone_set('Asia/Jakarta');
  		$datetime=date("YmdHis");
  		$tanggal_upload=date("Y-m-d");
  		$nip=$this->input->post('nip');
  		$id_ijin=$this->input->post('id_ijin');
  		$jenis_file=$this->input->post('jenis_file');
        $config['upload_path'] = './assets/uploads/';
        $config['allowed_types'] = 'jpg|jpeg|png|pdf'; 
        $config['file_name'] = $datetime;
        $config['max_size'] = 2048;
        $filename= $_FILES["berkas"]["name"];
		$file_ext = pathinfo($filename,PATHINFO_EXTENSION);
        $nama_lokasi=$datetime.".".$file_ext;
        // load library upload
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('berkas')) {
            $error = $this->upload->display_errors();
            // menampilkan pesan error
            redirect('Pengajuan_Controller/page/pelaporan_upload/'.$nip."/".$id_ijin."/".$jenis_file."/error");
            //print_r($error);
        } else {
            $result = $this->upload->data();
            $ary=array(
			'id_ijin'			=>$id_ijin,
			'jenis_file'		=>$jenis_file,
			'lokasi_file'		=>$nama_lokasi,
			'tanggal_upload'	=>$tanggal_upload
			);
			$this->Sop_Model->simpan_pelaporan('file_peserta',$ary);
            redirect('Pengajuan_Controller/page/pelaporan_status/'.$nip);
        }
	}

	function cetak_bukti($nip){
		//$hasil = $this->get_pegawai($nip);
		$ijin=$this->Sop_Model->qw("ijin_belajar.*, jenjang.jenjang AS nama_jenjang","ijin_belajar, jenjang","WHERE ijin_belajar.nip='$nip' AND ijin_belajar.jenjang=jenjang.id")->row_array();
		$pelapor = $this->get_pegawai($nip);
        $pdf = new FPDF('l','mm','A4');
        // membuat halaman baru
        $pdf->AddPage('P');
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','UB',16);
        // mencetak string 
        $pdf->Cell(190,7,'BUKTI PENDAFTARAN PERMOHONAN IJAZAH TINGGI',0,1,'C');
        $pdf->SetFont('Arial','B',11);
        $pdf->Cell(190,15,'',0,1);
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(40,7,'Yang bertanda tangan dibawah ini :',0,1);
        $pdf->Cell(190,3,'',0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->SetFont('Arial','B',11);
        $pdf->Cell(40,7,'NIP',0,0);
        $pdf->Cell(70,7,': '.$nip,0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->Cell(40,7,'NAMA',0,0);
        $pdf->Cell(70,7,': '.$pelapor['nama_pejabat'],0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->Cell(40,7,'GOL. / PANGKAT',0,0);
        $pdf->Cell(70,7,': '.$pelapor['golongan'].', '.$pelapor['pangkat'],0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->Cell(40,7,'UNIT KERJA',0,0);
        $pdf->Cell(70,7,': '.$pelapor['unker'],0,1);
        $pdf->Cell(190,3,'',0,1);
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(40,7,'Telah melakukan pendaftaran Permohonan Ijazah Tinggi dengan data sebagai berikut :',0,1);
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,3,'',0,1);
        $pdf->SetFont('Arial','B',11);
        $pdf->Cell(5,7,'',0,0);
        $pdf->Cell(40,7,'Sekolah / Univ.',0,0);
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(100,7,': '.$ijin['sekolah'],0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->SetFont('Arial','B',11);
        $pdf->Cell(40,7,'Jenjang',0,0);
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(100,7,': '.$ijin['nama_jenjang'],0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->SetFont('Arial','B',11);
        $pdf->Cell(40,7,'Prodi',0,0);
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(100,7,': '.$ijin['prodi'],0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->SetFont('Arial','B',11);
        $pdf->Cell(40,7,'Gelar Akademik',0,0);
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(100,7,': '.$ijin['gelar_akademik'],0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->SetFont('Arial','B',11);
        $pdf->Cell(40,7,'Nomor Ijazah',0,0);
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(100,7,': '.$ijin['nomor_ijazah'],0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->SetFont('Arial','B',11);
        $pdf->Cell(40,7,'Tanggal Ijazah',0,0);
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(100,7,': '.$ijin['tanggal_ijazah']."-".$ijin['bulan_ijazah']."-".$ijin['tahun_ijazah'],0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->SetFont('Arial','B',11);
        $pdf->Cell(40,7,'Tanggal Input',0,0);
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(70,7,': '.date("d-m-Y", strtotime($ijin['tanggal_input'])),0,1);
        $pdf->Cell(10,3,'',0,1);
        $pdf->Cell(100,7,'Demikian Bukti Pendaftaran ini supaya dapat digunakan sebagaimana mestinya',0,1);
        $pdf->Cell(10,3,'',0,1);
        $pdf->Cell(120,7,'',0,0);
        $pdf->Cell(40,7,'Kendal, '.date("d-m-Y", strtotime($ijin['tanggal_input'])),0,0);
        $pdf->Cell(10,30,'',0,1);
        $pdf->Cell(120,7,'',0,0);
        $pdf->SetFont('Arial','U',11);
        $pdf->Cell(40,7,$pelapor['nama_pejabat'],0,1,'C');
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(120,7,'',0,0);
        $pdf->Cell(40,7,$nip,0,0,'C');
        //$mahasiswa = $this->db->get('mahasiswa')->result();
        /*$getfile=$this->Sop_Model->qw("*","ijin_belajar","")->result();
        foreach ($getfile as $row){
            $pdf->Cell(20,6,$row->nip,1,0);
            $pdf->Cell(85,6,$row->sekolah,1,0);
            $pdf->Cell(27,6,$row->jenjang,1,0);
            $pdf->Cell(25,6,$row->prodi,1,1); 
        }*/
        $pdf->Output('','Bukti_Pendaftaran_'.$nip.'.pdf');
    }

	function simpan_user(){
		$username = $this->input->post('username');
		$ary=array(
			'username'	=>$this->input->post('username'),
			'password'	=>md5($this->input->post('password')),
			'level'		=>'1'
		);
		$this->Sop_Model->simpan_user('user',$ary);
		redirect('Tamu_Controller/page/pengguna_sukses/'.$username);
	}

	function simpan_pelaporan(){
		$nip = $this->input->post('nip');
		$edit = $this->input->post('edit');
		$admin = $this->input->post('admin');
		$status = $this->input->post('status');
		if($edit=='1')
		{
			$state=$status;
		}else{
			$state=0;
		}
		/*$hitung=$this->Sop_Model->qw('*','user',"WHERE nip='$nip' AND status='0'")->num_rows();
		if($hitung!='0')
		{
			redirect('Pengajuan_Controller/page/pelaporan/');
		}*/
		$tanggal_input = date("Y-m-d");

		$ary=array(
			'nip'				=>$this->input->post('nip'),
			'sekolah'			=>$this->input->post('sekolah'),
			'jenjang'			=>$this->input->post('jenjang'),
			'prodi'				=>$this->input->post('prodi'),
			'gelar_akademik'	=>$this->input->post('gelar_akademik'),
			'tipe_gelar'		=>$this->input->post('tipe_gelar'),
			'nomor_ijazah'		=>$this->input->post('nomor_ijazah'),
			'tanggal_ijazah' 	=>$this->input->post('tanggal_ijazah'),
			'bulan_ijazah'		=>$this->input->post('bulan_ijazah'),
			'tahun_ijazah'		=>$this->input->post('tahun_ijazah'),
			'tanggal_input'		=>$tanggal_input,
			'status'			=>$state
		);
		if($edit=='1')
		{
			$this->Sop_Model->edit_ijin('ijin_belajar',$nip,$ary);
			if($admin=='1')
			{
				redirect('Sop_Controller/page/lihat_pelaporan_status/'.$nip.'');
			}else{
				redirect('Pengajuan_Controller/page/pelaporan_status/'.$nip.'');
			}	
		}else{
			$this->Sop_Model->simpan_pelaporan('ijin_belajar',$ary);
			redirect('Pengajuan_Controller/page/pelaporan_tersimpan/'.$nip.'');
		}
		
	}

	function simpan_persetujuan(){
		$waktu = date("Y-m-d H:i:s");
		$sop = $this->input->post('id_sop');
		$ary=array(
			'id_sop'	=>$this->input->post('id_sop'),
			'tingkat'	=>$this->input->post('tingkat'),
			'oleh'		=>$this->input->post('oleh'),
			'waktu'		=>$waktu,
			'status'	=>$this->input->post('status'),
			'feedback'	=>$this->input->post('feedback')
		);
		$this->Sop_Model->simpan_persetujuan('persetujuan',$ary);
		redirect('Sop_Controller/page/persetujuan/'.$sop.'');
	}

	function edit_user(){
		$id=$this->input->post('id_user');
		$password=md5($this->input->post('password'));
		$ary=array(
			'id_user'	=>$this->input->post('id_user'),
			'username'	=>$this->input->post('username'),
			'password'	=>$password,
			'level'		=>$this->input->post('level')
		);
		$this->Sop_Model->edit_user('user',$id,$ary);
		redirect('Sop_Controller/page/user');
	}
	
	function hapus_user($id){
		$this->Sop_Model->hapus_user('user',$id);
		redirect('Sop_Controller/page/user');
	}
	
	
}
