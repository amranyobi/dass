<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();

        #SSO
        $this->sso_login = 'http://sisdm.semarangkota.go.id/api/sso_login';
        $this->sso_logout = 'http://sisdm.semarangkota.go.id/api/sso_logout';
        $this->sso_get_user = 'http://sisdm.semarangkota.go.id/api/user';
        $this->sso_get_pegawai = 'http://sisdm.semarangkota.go.id/api/monevaga?nip='; #PARAMETER NIP
        
        #END SSO


        /* load model */
        $this->load->model(array('db_model'));
        $this->load->library(array('pagination', 'form_validation', 'form_option', 'convertion'));
        $this->load->helper(array('url', 'form', 'date'));
        if ($this->session->userdata('id_pengguna_group') == 1) {
            echo "<script>location.href = '" . site_url("home2") . "';
		</script>";
        } else if ($this->session->userdata('id_pengguna_group') == 2) {
            echo "<script>location.href = '" . site_url("jawaban") . "';
		</script>";
        } else if ($this->session->userdata('id_pengguna_group') == 3) {
            echo "<script>location.href = '" . site_url("home2") . "';
		</script>";
        }
    }

    #========================================# SSO #========================================#
    
    public function sso_login()
    {
        #POST
        $nip    = $this->input->post('nip');
        $pass = $this->input->post('password');

        if(is_numeric($nip))
        {
            $par = [
                    'url'       => $this->sso_login,
                    'data'      => [
                                    'username'  => $nip,
                                    'password'  => $pass,
                                   ],
                   ];
            $res = $this->request_post($par);
            if($res->status == 'Success')
            {
                $dt = ['_token' => $res->token];
                $this->session->set_userdata($dt);

                $par2 = ['url' => $this->sso_get_user];
                $user = $this->request_get($par2);

                $par3 = ['url' => $this->sso_get_pegawai.$nip];
                $pegawai = $this->request_get($par3);

                $resx = new stdClass();

                // $resx->id_pengguna          = $user->id;
                $resx->nip                  = $pegawai->nip;
                $resx->id_pengguna_group    = 2;
                $resx->nama                 = $pegawai->nama;

                $sss = true;
                $xyz = true;
            }else{
                $pegawai = $this->get_sisdm($nip);

                if(isset($pegawai->kedudukan_pegawai))
                {
                    if($pegawai->kedudukan_pegawai == 'Pensiun')
                    {
                        $this->session->set_flashdata('salah', 'Pegawai sudah tidak aktif');
                    }else{
                        $this->session->set_flashdata('salah', 'NIP atau password salah');
                    }
                }else{
                    $this->session->set_flashdata('salah', 'NIP atau password salah');
                }
            }

            if(isset($xyz) && $xyz == true)
            {
                $qrx = $this->db_model->get('pengguna', '*', array('nip' => $nip));
                if ($qrx->num_rows() == 0) {
                    $abc = $this->pengguna_action($pegawai, 'insert');
                    if($abc){
                        if(isset($resx))
                        {
                            $qqx = $this->db_model->get('pengguna', 'id_pengguna', array('nip' => $nip));
                            if ($qqx->num_rows() == 0) {
                                $resx->id_pengguna = null;
                            }else{
                                $resx->id_pengguna = $qqx->row()->id_pengguna;
                            }
                        }
                    }
                }else{
                    $abc = $this->pengguna_action($pegawai, 'update');
                    if($abc){
                        if(isset($resx))
                        {
                            $resx->id_pengguna = $qrx->row()->id_pengguna;
                        }
                    }
                }
            }
        }else{
            $password = sha1(sha1(md5($pass)));
            $query = $this->db_model->get('pengguna', '*', array('nip' => $nip, 'status' => 1, 'password' => $password));
            if ($query->num_rows() == 0) {
                $this->session->set_flashdata('salah', 'NIP atau password salah');
            } else {
                $sss = true;
            }
        }

        if(isset($sss) && $sss == true)
        {
            if(isset($query))
            {
                $row = $query->row();
            }else{
                $row = $resx;
            }

            $login_data = array(
                'id_pengguna'           => $row->id_pengguna,
                'hash_id_pengguna'      => md5(sha1($row->id_pengguna)),
                'nip'                   => $row->nip,
                'id_pengguna_group'     => $row->id_pengguna_group,
                'nama'                  => $row->nama,
            );

            $this->session->set_userdata($login_data);

            if ($this->session->userdata('id_pengguna_group') == 1) { //administrator
                redirect('home2');
            } else if ($this->session->userdata('id_pengguna_group') == 2) { //data entri
                redirect('jawaban');
            } else {
                redirect('pengguna/tambah');
            }
        }else{
            redirect('login', 'refresh');
        }
        
    }

    public function sso_logout($par)
    {
        #GET
        $res = $this->request_get($par);
        return $res;
    }

    public function sso_get_user()
    {
        #GET
    }

    public function sso_get_pegawai()
    {
        #GET
    }

    public function request_post($par)
    {
        $data = $par['data'];
         
        $data_string = '';
        foreach($data as $key => $value) { $data_string .= $key.'='.$value.'&'; }
        rtrim($data_string, '&');
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $par['url']);
        curl_setopt($ch, CURLOPT_POST, count($data));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $responseJson = curl_exec( $ch );

        curl_close($ch);

        $result = json_decode($responseJson);
        return $result;
    }    

    public function request_get($par)
    {
        $data = array(
            'Content-Type: application/json',
            'Accept: application/json',
            'Authorization: Bearer ' . $this->session->userdata('_token'),
        );

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $par['url']);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $data);
        curl_setopt( $ch, CURLOPT_HEADER, 0);

        $result = curl_exec( $ch );

        curl_close($ch);

        if ($result === FALSE) {
            throw new Exception('CURL Error: ' . curl_error($ch), curl_errno($ch));
        }else{
            $obj = json_decode($result);
            return $obj;
        }
    }

    #========================================# END SSO #========================================#

    public function index() {
        $data['nip_error'] = '';
        $data['salah'] = '';
        $nip = $this->input->post('nip');
        $password = sha1(sha1(md5($this->input->post('password'))));
        if ($nip != '') {
            $query = $this->db_model->get('pengguna', '*', array('nip' => $nip));
            if ($query->num_rows() == 0) {
                $sd = $this->get_sisdm($nip);
                if($sd != null)
                {
                    $this->pengguna_action($sd, 'insert');
                    $query2 = $this->db_model->get('pengguna', '*', array('nip' => $nip));
                    $sss = true;
                }else{
                    $data['salah'] = 'NIP atau password salah';
                }
            } else {
                $query = $this->db_model->get('pengguna', '*', array('nip' => $nip, 'status' => 0));
                if ($query->num_rows() > 0) {
                    $data['nip_error'] = 'NIP tidak aktif';
                } else {
                    $query = $this->db_model->get('pengguna', '*', array('nip' => $nip, 'status' => 1, 'password' => $password));
                    if ($query->num_rows() == 0) {
                        $data['salah'] = 'NIP atau password salah';
                    } else {
                        $sd = $this->get_sisdm($nip);
                        $this->pengguna_action($sd, 'update');
                        $query2 = $this->db_model->get('pengguna', '*', array('nip' => $nip));
                        $sss = true;
                    }
                }
            }
        }

        if(isset($sss) && $sss == true)
        {
            $row = $query2->row();
            $login_data = array(
                'id_pengguna' => $row->id_pengguna,
                'hash_id_pengguna' => md5(sha1($row->id_pengguna)),
                'nip' => $row->nip,
                'id_pengguna_group' => $row->id_pengguna_group,
                'nama' => $row->nama
            );
            $this->session->set_userdata($login_data);
                        if ($row->id_pengguna_group == 1) { //administrator
                            redirect('home2');
                        } else if ($row->id_pengguna_group == 2) { //data entri
                            redirect('jawaban');
                        } else {
                            redirect('pengguna/tambah');
                        }
        }
        //$data['content'] = $this->load->view('login/main', $data, true);
        $this->load->view('login/main', $data);
    }

    public function pengguna_action($obj, $tp)
    {
        $data = [
                           'nama'                   => strtoupper($obj->nama),
                           'jabatan'                => strtoupper($obj->jabatan),
                           'perangkat_daerah'       => strtoupper($obj->opd),
                           'unit_kerja'             => strtoupper($obj->opd),
                           'satuan_kerja'           => strtoupper($obj->opd),
                           'jenis_kelamin'          => (strtolower($obj->jenis_kelamin) == strtolower('Laki-Laki')) ? 1 : 2,
                           'pendidikan_terakhir'    => $this->getForeign($obj->pendidikan_terakhir, 'pendidikan'),
                           'eselonering'            => $this->getForeign($obj->golongan, 'eselonering'),
                           'usia'                   => $this->usia($obj->tanggal_lahir),
                           'tgl_lahir'              => $obj->tanggal_lahir,
                           'tempat_lahir'           => $obj->tempat_lahir,
                        ];
                        ////////////////////////
                           // 'pangkat'                => $obj->pangkat,
                           // 'golongan'               => $obj->golongan,
                           // 'rumpun_jabatan'         => $obj->rumpun_jabatan,
        
        // var_dump($obj);
        $ci = &get_instance();

        if($tp == 'insert')
        {
            $data['nip']                    = $obj->nip;
            $data['password']               = sha1(sha1(md5('123456')));
            $data['tanggal_registrasi']     = date('Y-m-d');
            $data['status']                 = 1;
            $data['eselon2']                = null;
            $data['id_pengguna_group']      = 3;

            $cx = $ci->db->insert('pengguna', $data);
        }

        if($tp == 'update')
        {
            $cx = $ci->db->where('nip', $obj->nip)->update('pengguna', $data);
        }

        return $cx;
    }
    
    public function get_sisdm($nipx = '')
    {
    	$result_token 	= $this->get_token();

    	if($result_token['status'] == true)
    	{
	    	$nip 	= $nipx; 

	    	$url = 'http://sisdm.semarangkota.go.id/api/monevaga?nip='. $nip;

	    	$data = array(
		        'Content-Type: application/json',
		        'Accept: application/json',
		        'Authorization: '.$result_token['_token_type'].' ' . $result_token['_token']
		    );

	    	$ch = curl_init();
			curl_setopt( $ch, CURLOPT_URL, $url);
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt( $ch, CURLOPT_HTTPHEADER, $data);
			curl_setopt( $ch, CURLOPT_HEADER, 0);

			$result = curl_exec( $ch );

			curl_close($ch);

			if ($result === FALSE) {
				throw new Exception('CURL Error: ' . curl_error($ch), curl_errno($ch));
			}else{
				$obj = json_decode($result);
				return $obj;
			}
		}else{
			$result = null;
		}
    }

    public function get_token()
    {
    	$url = 'http://sisdm.semarangkota.go.id/oauth/token';

    	$data['grant_type'] 	= 'password';
    	$data['client_id'] 		= '5';
    	$data['client_secret'] 	= 'gcEBVCjvi6QQRdXtaALozh2zA8gsRnv5fhngTikl';
    	$data['username'] 		= 'bagianorganisasi';
    	$data['password'] 		= 'menjemputrezeki';

		$data_string = '';
		foreach($data as $key => $value) { $data_string .= $key.'='.$value.'&'; }
		rtrim($data_string, '&');
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, count($data));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$responseJson = curl_exec( $ch );

		curl_close($ch);

		$result = json_decode($responseJson);

		if(!isset($result->error))
		{
			$token_type 	= $result->token_type;
			$access_token 	= $result->access_token;
			$auth 			= $token_type.' '.$access_token;

			$respon = ['status' => true, '_token' => $access_token, '_token_type' => $token_type];
		}else{
			$respon = ['status' => false, 'message' => $result->message];
		}
		return $respon;
    }

    public function logout() {
//        if ($this->session->userdata('id_pengguna') == null) {
//            echo "<script>location.href = '" . base_url('index.php') . "/login';
//		</script>";
//            
//        }
//        $this->session->sess_destroy();
//        echo "<script>location.href = '" . base_url('index.php') ."/login';
//			</script>";

        if($this->session->userdata('_token') !== null)
        {
            $par = ['url' => $this->sso_logout];
            $res = $this->sso_logout($par);

            if($res->status == 'Success')
            {
                $xxx = true;
            }else{
                $xxx = false;
            }
        }else{
            if ($this->session->userdata('id_pengguna') == null) {
                redirect('login');
            }
            
            $xxx = true;
        }
        $this->session->sess_destroy();
        
        if($xxx)
        {
            redirect('login');
        }else{
            echo "<script>alert('Cannot log out');</script>";
        }
    }

    public function usia($lahir)
    {
        $birthDt = new DateTime($lahir); //tanggal hari ini
                        $today = new DateTime('today'); //tahun
                        $tahun = $today->diff($birthDt)->y;

                        if ($tahun > 50)
                            $usia = 1;
                        else if ($tahun >= 46 && $tahun <= 50)
                            $usia = 2;
                        else if ($tahun >= 41 && $tahun <= 45)
                            $usia = 3;
                        else if ($tahun >= 36 && $tahun <= 40)
                            $usia = 4;
                        else if ($tahun >= 31 && $tahun <= 35)
                            $usia = 4;
                        else if ($tahun <= 30)
                            $usia = 6;
        return $usia;
    }

    public function getForeign($par1, $par2)
    {
        $ci = &get_instance();
        if($par2 == 'pendidikan')
        {
            $parx = explode(' ', $par1);
            if(isset($parx[0]))
            {
                if($parx[0] == 'SMA')
                {
                    $parux = 'SLTA';
                }elseif($parx[0] == 'SMP'){
                    $parux = 'SLTP';
                }else{
                    $parux = $parx[0];
                }
                $res = $ci->db->select('*')->from('pendidikan')->where('LOWER(pendidikan) like', '%'.strtolower($parux).'%')->limit(1)->get()->row();
                if($res != null)
                {
                    return $res->id_pendidikan;
                }else{
                    return null;
                }
            }else{
                return null;
            }
        }

        if($par2 == 'eselonering')
        {
            $res = $ci->db->select('*')->from('eselonering')->where('LOWER(eselon) like', '%'.strtolower($par1).'%')->limit(1)->get()->row();
            
            if($res != null)
            {
                return $res->id_eselon;
            }else{
                return null;
            }

        }
    }

}