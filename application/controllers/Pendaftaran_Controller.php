<?php
class Pendaftaran_Controller extends CI_Controller {
	function __construct(){
		parent:: __construct();
		/*
		if ($this->session->userdata('nama')=="") {
	 		 if($this->session->userdata('level')!='1'){
	 				 if($this->session->userdata('level')!='5'){
	 				 	redirect('LoginAdmin_Controller');
	 				 }
	 		 }
	 	 }*/
		$this->load->model('Sop_Model');
		// $this->load->library('pdf');
		$this->load->helper(array('form', 'url'));
		
	}

  function home(){
    $data['page']='pendaftaran_siswa';
    $data['open']='Pendaftaran_Controller/simpan_datasiswa';
    $this->load->view('guest',$data);
  }

	function page(){
		$page=$this->uri->segment(3);
		$data['page']=$page;
		if($page=="user"){
			$data['tmp_pt']=$this->Sop_Model->qw("*","user","")->result();
		}elseif($page=="persetujuan"){
			$yy=$this->uri->segment(4);
			$data['tmp_pt']=$this->Sop_Model->qw("*","persetujuan","WHERE id_sop= '$yy'")->result();
			$data['sop']=$this->Sop_Model->qw("*","sop","WHERE no_sop= '$yy'");
		}elseif($page=="identitas_aplikasi"){
			$data['tmp_pt']=$this->Sop_Model->qw("*","identitas_aplikasi","")->result();
		}elseif($page=="pelaporan"){
			$data['open']='Pengajuan_Controller/page/pelaporan_siap';
		}elseif($page=="pelaporan_cek"){
			$data['open']='Pengajuan_Controller/page/pelaporan_status';
		}elseif($page=="biodata_survey"){
			$data['open']='Pendaftaran_Controller/simpan_biodata';
		}elseif($page=="data_checklist"){
			$data['tmp_pt']=$this->Sop_Model->qw("*","data_pernyataan","ORDER BY nomor ASC")->result();
			$data['open']='Pendaftaran_Controller/simpan_pernyataan';
		}elseif($page=="pelaporan_status"){
			$nip=$this->input->post('nip');
			if(!isset($nip))
			{
				$nip=$this->uri->segment(4);
			}
			$data['data_pegawai'] = $this->get_pegawai($nip);
		}elseif($page=="pelaporan_upload"){
			$data['nip']=$this->uri->segment(4);
			$data['id_ijin']=$this->uri->segment(5);
			$data['jenis_file']=$this->uri->segment(6);
			$data['open']='Pengajuan_Controller/pelaporan_unggah';
		}elseif($page=='pelaporan_tersimpan'){
			$nip=$this->uri->segment(4);
			$data['data_pegawai'] = $this->get_pegawai($nip);
		}
		$this->load->view('guest',$data);
	}


	function detail($xx){
		$no_sop=$this->uri->segment(4);
		if(empty($no_sop)){
         $data['dt']=$this->Sop_Model->qw("detail_pelaksana.id_pelaksana,detail_pelaksana.no_sop,pelaksana.nama_pelaksana","pelaksana","inner join detail_pelaksana on detail_pelaksana.id_pelaksana=pelaksana.id_pelaksana WHERE detail_pelaksana.no_sop='$xx'")->result();
		}else{
			$data['dt']=$this->Sop_Model->qw("detail_pelaksana.id_pelaksana,detail_pelaksana.no_sop,pelaksana.nama_pelaksana","pelaksana","inner join detail_pelaksana on detail_pelaksana.id_pelaksana=pelaksana.id_pelaksana WHERE detail_pelaksana.no_sop='$no_sop'")->result();
		}
        $this->load->view('content/detail_pelaksana.php',$data);
	}

	function get_pegawai($nip){
		  $url = 'https://simpeg.kendalkab.go.id/v17/api/pegawaidetail/'.$nip;
          $curl = curl_init();
          curl_setopt_array($curl, array(
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 60,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              CURLOPT_HTTPHEADER => array(
                  "content-type: application/x-www-form-urlencoded"
              ),
              CURLOPT_URL => $url
          ));

          $response = curl_exec($curl);
          $curl_errno = curl_errno($curl);
          $curl_error = curl_error($curl);
          curl_close($curl);
          $svcpegawai = json_decode($response, true);
          $coba = $svcpegawai['pegawaidetail'][0];
          return $coba;
	}

	function lokasi_file($jenis_file,$id_ijin)
	{
		$getfile=$this->Sop_Model->qw("*","file_peserta","WHERE id_ijin='$id_ijin' AND jenis_file='$jenis_file'")->row_array();
		return $getfile;
	}

	function pelaporan_unggah()
	{
		// setting konfigurasi upload
		date_default_timezone_set('Asia/Jakarta');
  		$datetime=date("YmdHis");
  		$tanggal_upload=date("Y-m-d");
  		$nip=$this->input->post('nip');
  		$id_ijin=$this->input->post('id_ijin');
  		$jenis_file=$this->input->post('jenis_file');
        $config['upload_path'] = './assets/uploads/';
        $config['allowed_types'] = 'jpg|jpeg|png|pdf'; 
        $config['file_name'] = $datetime;
        $config['max_size'] = 2048;
        $filename= $_FILES["berkas"]["name"];
		$file_ext = pathinfo($filename,PATHINFO_EXTENSION);
        $nama_lokasi=$datetime.".".$file_ext;
        // load library upload
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('berkas')) {
            $error = $this->upload->display_errors();
            // menampilkan pesan error
            redirect('Pengajuan_Controller/page/pelaporan_upload/'.$nip."/".$id_ijin."/".$jenis_file."/error");
            //print_r($error);
        } else {
            $result = $this->upload->data();
            $ary=array(
			'id_ijin'			=>$id_ijin,
			'jenis_file'		=>$jenis_file,
			'lokasi_file'		=>$nama_lokasi,
			'tanggal_upload'	=>$tanggal_upload
			);

			$ary_update=array(
			'lokasi_file'		=>$nama_lokasi,
			'tanggal_upload'	=>$tanggal_upload,
			'revisi'	=>1
			);

            //hitung update
            $sudah=$this->Sop_Model->qw('*','file_peserta',"WHERE id_ijin='$id_ijin' AND jenis_file='$jenis_file'")->num_rows();
            if($sudah=='0')
				$this->Sop_Model->simpan_pelaporan('file_peserta',$ary);
			else
				$this->Sop_Model->edit_pelaporan('file_peserta',$id_ijin,$jenis_file,$ary_update);

            redirect('Pengajuan_Controller/page/pelaporan_status/'.$nip);
        }
	}

	function cetak_bukti($id){
		  $id_siswa=$id;
          $val=$this->Sop_Model->qw("*","data_jawaban","WHERE id_siswa='$id_siswa'")->result();
          $data_peserta=$this->Sop_Model->qw("*","data_peserta","WHERE id='$id_siswa'")->row_array();
          $arr = explode(' ',trim($data_peserta['nama']));
		  $judul_pdf = $arr[0].'_'.$id_siswa;
          $waktu_input = date("d-m-Y", strtotime($data_peserta['waktu_input']));
          $lin=0;
          $mu=0;
          $lm=0;
          $nter=0;
          $sp=0;
          $ntra=0;
          $bk=0;
          $nat=0;
          foreach($val as $tampil){
            $pernyataan=$this->Sop_Model->qw("*","data_pernyataan","WHERE nomor='$tampil->id_pernyataan'"
        )->row_array();
            if($tampil->jawaban=='1' AND $pernyataan['kecerdasan']=='LIN')
              $lin++;
            if($tampil->jawaban=='1' AND $pernyataan['kecerdasan']=='MU')
              $mu++;
            if($tampil->jawaban=='1' AND $pernyataan['kecerdasan']=='LM')
              $lm++;
            if($tampil->jawaban=='1' AND $pernyataan['kecerdasan']=='NTER')
              $nter++;
            if($tampil->jawaban=='1' AND $pernyataan['kecerdasan']=='SP')
              $sp++;
            if($tampil->jawaban=='1' AND $pernyataan['kecerdasan']=='NTRA')
              $ntra++;
            if($tampil->jawaban=='1' AND $pernyataan['kecerdasan']=='BK')
              $bk++;
            if($tampil->jawaban=='1' AND $pernyataan['kecerdasan']=='NAT')
              $nat++;
          }

          $p_lin = $lin/10*100;
          $p_mu = $mu/10*100;
          $p_lm = $lm/10*100;
          $p_nter = $nter/10*100;
          $p_sp = $sp/10*100;
          $p_ntra = $ntra/10*100;
          $p_bk = $bk/10*100;
          $p_nat = $nat/10*100;

          $data_prodi=array();
          //masukkan kondisi prodi
          //FK dihilangkan
          /*
          if($p_lm>80 && $p_bk>30 && $p_nter>50 && $p_nat>50)
            $data_prodi[1] = 1;
          if($p_lm>70 && $p_bk>30 && $p_nter>50 && $p_nat>50)
            $data_prodi[2] = 2;*/
          if($p_lm>60 && $p_nter>50 && $p_nat>50)
            $data_prodi[3] = 3;
          if($p_lm>70 && $p_nter>70 && $p_nat>60)
            $data_prodi[4] = 4;
          if($p_lm>60 && $p_nter>50 && $p_nat>60)
            $data_prodi[5] = 5;
          if($p_lm>60 && $p_nter>50 && $p_nat>60)
            $data_prodi[6] = 6;
          if($p_lm>60 && $p_sp>40 && $p_nter>40)
            $data_prodi[7] = 7;
          if($p_lm>50 && $p_sp>50 && $p_nter>50 && $p_ntra>30 && $p_nat>30)
            $data_prodi[8] = 8;
          if($p_lm>60 && $p_nter>50 && $p_ntra>30)
            $data_prodi[9] = 9;
          if($p_lm>65)
            $data_prodi[10] = 10;
          if($p_lm>65)
            $data_prodi[11] = 11;
          if($p_lm>40 && $p_sp>30 && $p_nter>30)
            $data_prodi[12] = 12;
          if($p_lm>40 && $p_sp>30 && $p_nter>30)
            $data_prodi[13] = 13;
          if($p_lin>60 && $p_nter>50)
            $data_prodi[14] = 14;
          if($p_lin>60 && $p_nter>60)
            $data_prodi[15] = 15;
          if($p_lm>50 && $p_nter>50 && $p_nat>60)
            $data_prodi[16] = 16;
          if($p_lm>60 && $p_nter>50)
            $data_prodi[17] = 17;
          if($p_lm>60 && $p_nter>70 && $p_nat>60)
            $data_prodi[18] = 18;
          if($p_lm>50 && $p_nter>60 && $p_nat>60)
            $data_prodi[19] = 19;
          if($p_lm>60 && $p_nter>70 && $p_nat>60)
            $data_prodi[20] = 20;
          if($p_lm>60 && $p_nter>70 && $p_nat>60)
            $data_prodi[21] = 21;
          if($p_lm>60 && $p_nter>70 && $p_nat>60)
            $data_prodi[22] = 22;

          if(empty($data_prodi))
          {
            $data_prodi[3] = 3;
            $data_prodi[5] = 5;
            $data_prodi[6] = 6;
            $data_prodi[12] = 12;
            $data_prodi[13] = 13;
            $data_prodi[14] = 14;
            $data_prodi[15] = 15;
            $data_prodi[16] = 16;
            $data_prodi[19] = 19;
            $data_prodi[20] = 20;
          }

          $data[] = array('nilai' => $p_lin, 'tipe' => 'LIN');
          $data[] = array('nilai' => $p_mu, 'tipe' => 'MU');
          $data[] = array('nilai' => $p_lm, 'tipe' => 'LM');
          $data[] = array('nilai' => $p_nter, 'tipe' => 'NTER');
          $data[] = array('nilai' => $p_sp, 'tipe' => 'SP');
          $data[] = array('nilai' => $p_ntra, 'tipe' => 'NTRA');
          $data[] = array('nilai' => $p_bk, 'tipe' => 'BK');
          $data[] = array('nilai' => $p_nat, 'tipe' => 'NAT');

          foreach ($data as $key => $row) {
              $nilai[$key]  = $row['nilai'];
              $tipe[$key] = $row['tipe'];
          }

          $nilai  = array_column($data, 'nilai');
          array_multisort($nilai, SORT_DESC, $data);

          foreach ($data[0] as $key => $value) {
            if ($key == "nilai")
              $nilai1 = $value;
            if ($key == "tipe")
              $tipe1 = $value;
          }

          foreach ($data[1] as $key => $value) {
            if ($key == "nilai")
              $nilai2 = $value;
            if ($key == "tipe")
              $tipe2 = $value;
          }

          foreach ($data[2] as $key => $value) {
            if ($key == "nilai")
              $nilai3 = $value;
            if ($key == "tipe")
              $tipe3 = $value;
          }

        $ambil_cerdas=$this->Sop_Model->qw("*","kecerdasan","WHERE istilah='$tipe1'")->row_array();
        $ambil_cerdas2=$this->Sop_Model->qw("*","kecerdasan","WHERE istilah='$tipe2'")->row_array();
        $ambil_cerdas3=$this->Sop_Model->qw("*","kecerdasan","WHERE istilah='$tipe3'")->row_array();
        $isi_prodi = '';
        $terakhir = end($data_prodi);
        foreach ($data_prodi as $key => $value) {
                      $ambil_nama=$this->Sop_Model->qw("prodi","prodi","WHERE id='$value'")->row_array();
                      if($terakhir==$value)
                      	$isi_prodi .= $ambil_nama['prodi'];
                      else
                      	$isi_prodi .= $ambil_nama['prodi'].', ';
                    }
        $image1 = "./assets/image/logo.jpg";
        $image2 = "./assets/image/header.jpg";
        $footer = "./assets/image/footer.jpg";

		$datam = array('Linguistic' => $p_lin, 'Musical' => $p_mu, 'Logical-Mathematical' => $p_lm, 'Interpersonal' => $p_nter, 'Spatial' => $p_sp, 'Intrapersonal' => $p_ntra, 'Bodily-Kinesthetic' => $p_bk, 'Naturalist' => $p_nat);

        $pdf = new FPDF('l','mm','A4');
        // membuat halaman baru
        $pdf->AddPage('P');
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',14);
        // mencetak string 

        //$pdf->Image($image1, 14, 2, 30);
        $pdf->Image($image2, 0, 0, 210);
        /*$pdf->Cell(40,3,'',0,0);
        $pdf->Cell(190,3,'UPT. ADMISI UNIVERSITAS MUHAMMADIYAH SEMARANG',0,1);
        $pdf->Cell(40,7,'',0,0);
        $pdf->SetFont('Arial','',11);
        $pdf->Cell(190,7,'Jl. Kedungmundu Raya No.18, Semarang. Telp. (024) 76740296, 76740297',0,1);
        $pdf->Cell(40,7,'',0,0);
        $pdf->Cell(190,4,'Fax (024) 76740291, Website : unimus.ac.id',0,1);
        $pdf->SetLineWidth(0.2);
        $pdf->Line(10,32,200,32);
        $pdf->SetLineWidth(0.5);
        $pdf->Line(10,33,200,33);*/
        $pdf->SetFont('Arial','B',11);
        $pdf->Cell(190,29,'',0,1);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(190,3,'THE MULTIPLE INTELLIGENCES CHECKLIST',0,1,'C');
        $pdf->Cell(190,3,'',0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(40,4,'NAMA',0,0);
        $pdf->Cell(70,4,': '.$data_peserta['nama'],0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->Cell(40,7,'SEKOLAH',0,0);
        $pdf->Cell(70,7,': '.$data_peserta['sekolah'],0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->Cell(40,4,'NO. HANDPHONE',0,0);
        $pdf->Cell(70,4,': '.$data_peserta['hp'],0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->Cell(40,7,'EMAIL',0,0);
        $pdf->Cell(70,7,': '.$data_peserta['email'],0,1);
        $pdf->Cell(190,2,'',0,1);

        $pdf->SetFont('Arial','B',7);
        $width_cell=array(23,23,23,23,23,23,23,23);
    		$pdf->SetFillColor(51,102,255); 
    		$pdf->Cell($width_cell[0],7,'Linguistic',1,0,'C',true); 
    		$pdf->SetFillColor(255,51,51); 
    		$pdf->Cell($width_cell[1],7,'Musical',1,0,'C',true);
    		$pdf->SetFillColor(255,204,51); 
    		$pdf->Cell($width_cell[2],7,'Log.Mathematical',1,0,'C',true);
    		$pdf->SetFillColor(51,255,102);
    		$pdf->Cell($width_cell[3],7,'Interpersonal',1,0,'C',true); 
    		$pdf->SetFillColor(255,102,255); 
    		$pdf->Cell($width_cell[4],7,'Spatial',1,0,'C',true);  
    		$pdf->SetFillColor(255,102,0); 
    		$pdf->Cell($width_cell[5],7,'Intrapersonal',1,0,'C',true); 
    		$pdf->SetFillColor(204,51,204);
    		$pdf->Cell($width_cell[6],7,'Bod.Kinesthetic',1,0,'C',true);
    		$pdf->SetFillColor(153,153,153); 
    		$pdf->Cell($width_cell[7],7,'Naturalist',1,1,'C',true); 
    		//// header is over ///////

    		$pdf->SetFont('Arial','',8);
    		// First row of data 
    		$pdf->Cell($width_cell[0],7,$p_lin,1,0,'C',false); // First column of row 1 
    		$pdf->Cell($width_cell[1],7,$p_mu,1,0,'C',false); // Second column of row 1 
    		$pdf->Cell($width_cell[2],7,$p_lm,1,0,'C',false); // Third column of row 1 
    		$pdf->Cell($width_cell[3],7,$p_nter,1,0,'C',false); // Fourth column of row 1 
    		$pdf->Cell($width_cell[4],7,$p_sp,1,0,'C',false); // First column of row 1 
    		$pdf->Cell($width_cell[5],7,$p_ntra,1,0,'C',false); // First column of row 1 
    		$pdf->Cell($width_cell[6],7,$p_bk,1,0,'C',false); // First column of row 1 
    		$pdf->Cell($width_cell[7],7,$p_nat,1,1,'C',false); // First column of row 1 

    		$pdf->Cell(190,8,'',0,1);
    		$pdf->Cell(40,20,'',0,0);
            $col1=array(51,102,255);
    		$col2=array(255,51,51);
    		$col3=array(255,204,51);
    		$col4=array(51,255,102);
    		$col5=array(255,102,255);
    		$col6=array(255,102,0);
    		$col7=array(204,51,204);
    		$col8=array(153,153,153);
    		$pdf->PieChart(100, 30, $datam, '%l (%p)', array($col1,$col2,$col3,$col4,$col5,$col6,$col7,$col8));



        $pdf->Cell(190,3,'',0,1);
        $pdf->Cell(10,3,'',0,1);
        $pdf->SetFont('Arial','B',10);
        //$pdf->Cell(5,4,'',0,0);
        /*$pdf->Cell(40,4,'Nilai Komponen',0,0);
        $pdf->SetFont('Arial','',10);
        $pdf->Cell(100,4,': Linguistic : '.$p_lin.', Musical : '.$p_mu.', Logical-Mathematical : '.$p_lm.', Interpersonal : '.$p_nter.'',0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(40,7,'',0,0);
        $pdf->SetFont('Arial','',10);
        $pdf->Cell(100,7,'  Spatial : '.$p_sp.', Intrapersonal : '.$p_ntra.', Bodily-Kinesthetic : '.$p_bk.', Naturalist : '.$p_nat.'',0,1);*/
        $pdf->Cell(5,7,'',0,0);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(40,7,'KECERDASAN MENONJOL (3 TERATAS)',0,0);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(190,6,'',0,1);
        $pdf->Cell(5,7,'',0,0);
        $pdf->Cell(100,7,'1. '.$ambil_cerdas['nama_kecerdasan'],0,1);
        $pdf->Cell(5,4,'',0,0);
        $pdf->Cell(4,4,'',0,0);
        $pdf->SetFont('Arial','',8);
        $pdf->MultiCell(180,4,''.$ambil_cerdas['penjelasan'],0,1);
        $pdf->Cell(9,4,'',0,0);
        $pdf->SetFont('Arial','B',8);
        $pdf->MultiCell(180,4,'Pilihan Karier : '.$ambil_cerdas['karir'],0,1);
        $pdf->Cell(190,1,'',0,1);
        $pdf->Cell(5,10,'',0,0);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(100,7,'2. '.$ambil_cerdas2['nama_kecerdasan'],0,1);
        $pdf->Cell(5,2,'',0,0);
        $pdf->Cell(4,4,'',0,0);
        $pdf->SetFont('Arial','',8);
        $pdf->MultiCell(180,4,''.$ambil_cerdas2['penjelasan'],0,1);
        $pdf->Cell(9,4,'',0,0);
        $pdf->SetFont('Arial','B',8);
        $pdf->MultiCell(180,4,'Pilihan Karier : '.$ambil_cerdas2['karir'],0,1);
        $pdf->Cell(190,1,'',0,1);
        $pdf->Cell(5,10,'',0,0);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(100,7,'3. '.$ambil_cerdas3['nama_kecerdasan'],0,1);
        $pdf->Cell(5,2,'',0,0);
        $pdf->Cell(4,4,'',0,0);
        $pdf->SetFont('Arial','',8);
        $pdf->MultiCell(180,4,''.$ambil_cerdas3['penjelasan'],0,1);
        $pdf->Cell(9,4,'',0,0);
        $pdf->SetFont('Arial','B',8);
        $pdf->MultiCell(180,4,'Pilihan Karier : '.$ambil_cerdas3['karir'],0,1);
        $pdf->Cell(190,4,'',0,1);
        $pdf->Cell(5,4,'',0,0);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(40,4,'ALTERNATIF PILIHAN PROGRAM STUDI',0,0);
        $pdf->Cell(190,5,'',0,1);
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(5,4,'',0,0);
        $pdf->MultiCell(180,4,$isi_prodi,0,1);
        $pdf->Cell(190,5,'',0,1);
        $pdf->Cell(5,4,'',0,0);
        $pdf->SetFont('Arial','BI',10);
        $pdf->SetTextColor(0,80,180);
        $pdf->Cell(180,4,'Hasil tes dapat dipakai dasar Anda untuk diterima sebagai mahasiswa baru melalui jalur bebas tes tulis (JBTT)',0,0,'C');
        $pdf->Image($footer, 0, 250, 210);
        $pdf->Cell(10,3,'',0,1);
        $pdf->Output('I','Hasil_MinatBakat_'.$judul_pdf.'.pdf');
        //$pdf->Output('D','Hasil_MinatBakat_'.$judul_pdf.'.pdf');
    }

	function simpan_user(){
		$username = $this->input->post('username');
		$hitung=$this->Sop_Model->qw('*','user',"WHERE username='$username'")->num_rows();
		if($hitung!='0')
		{
			redirect('Sop_Controller/page/user/');
		}
		$ary=array(
			'id_user'	=>$this->input->post('id_user'),
			'username'	=>$this->input->post('username'),
			'password'	=>md5($this->input->post('password')),
			'level'		=>$this->input->post('level')
		);
		$this->Sop_Model->simpan_user('user',$ary);
		redirect('Sop_Controller/page/user');
	}

	function simpan_biodata(){
		$nama = $this->input->post('nama');
		$email = $this->input->post('email');
    $status = $this->input->post('status');
    $nimnik = $this->input->post('nimnik');
    $angkatan = $this->input->post('angkatan');
		$waktu_input = date("Y-m-d H:i:s");

		$ary=array(
			'nama'				=>$this->input->post('nama'),
			'email'			=>$this->input->post('email'),
      'status'     =>$this->input->post('status'),
			'nimnik'				=>$this->input->post('nimnik'),
			'angkatan'				=>$this->input->post('angkatan'),
			'waktu_input'		=>$waktu_input
		);

		$this->Sop_Model->simpan_biodata('data_peserta',$ary);
		$id = $this->db->insert_id();
		redirect('Pendaftaran_Controller/page/data_checklist_vmts/'.$id.'');
		
	}

	function simpan_pernyataan(){
		$nim = $this->input->post('nim');
    $jenis_kuesioner = $this->input->post('jenis_kuesioner');
		//$halaman = $this->input->post('halaman');
		//$next = $halaman+1;

		foreach ($this->input->post('no_pernyataan') as $key => $value) {
			
			$jawaban = $this->input->post('pernyataan')[$value];
			$ary=array(
				'nim'			=>$nim,
				'jenis_kuesioner'     =>$jenis_kuesioner,
        'id_pernyataan'		=>$value,
				'jawaban'			=>$jawaban
			);
			$this->Sop_Model->simpan_jawaban('data_jawaban',$ary);
		}
		//if($halaman=='8')
		//	redirect('Pendaftaran_Controller/page/hasil_survey/'.$id_siswa.'');
		//else
			redirect('Pendaftaran_Controller/page/data_checklist/'.$jenis_kuesioner);
	}

  function simpan_pernyataan_vmts(){
    $id_peserta = $this->input->post('id_peserta');
    $komentar = $this->input->post('komentar');

    foreach ($this->input->post('no_pernyataan') as $key => $value) {
      
      $jawaban = $this->input->post('pernyataan')[$value];
      $ary=array(
        'id_peserta'     =>$id_peserta,
        'id_pernyataan'   =>$value,
        'jawaban'     =>$jawaban,
        'komentar'     =>$komentar
      );
      $this->Sop_Model->simpan_jawaban('data_jawaban_vmts',$ary);
    }
    //if($halaman=='8')
    //  redirect('Pendaftaran_Controller/page/hasil_survey/'.$id_siswa.'');
    //else
      redirect('Pendaftaran_Controller/page/terima_kasih/');
  }

	function edit_user(){
		$id=$this->input->post('id_user');
		$password=md5($this->input->post('password'));
		$ary=array(
			'id_user'	=>$this->input->post('id_user'),
			'username'	=>$this->input->post('username'),
			'password'	=>$password,
			'level'		=>$this->input->post('level')
		);
		$this->Sop_Model->edit_user('user',$id,$ary);
		redirect('Sop_Controller/page/user');
	}

	function ubah_sandi(){
		$username=$this->input->post('username');
		$password=md5($this->input->post('password'));
		$ary=array(
			'password'	=>$password
		);
		$this->Sop_Model->reset_password('user',$username,$ary);
		redirect('Pengajuan_Controller/page/ubah_sandi/sukses');
	}
	
	function hapus_user($id){
		$this->Sop_Model->hapus_user('user',$id);
		redirect('Sop_Controller/page/user');
	}

	function logout(){
		$this->session->sess_destroy();
		redirect('LoginAdmin_Controller');
	}
	

	/*public function cetak(){
		ob_start();
		$data['siswa'] = $this->siswa_model->view_row();
		$this->load->view('print', $data);
		$html = ob_get_contents();
		ob_end_clean();
		require_once('./assets/html2pdf/html2pdf.class.php');
		$pdf = new HTML2PDF('P','A4','en');
		$pdf->WriteHTML($html);
		$pdf->Output('Cetak SOP.pdf', 'D');
	}*/
	
}
